﻿using FrontEndVakyria.Modelos_BD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEndVakyria.Models
{
    public class LogModelo
    {

        public int IngresarLog(Log log)
        {

            using (var db = new BD_ProyectoValkyriaContext())
            {
                db.Logs.Add(log);
                return db.SaveChanges();

            }

        }

    }
}
