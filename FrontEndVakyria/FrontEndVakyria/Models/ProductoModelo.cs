﻿using FrontEndVakyria.Entities;
using FrontEndVakyria.Modelos_BD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEndVakyria.Models
{
    public class ProductoModelo
    {

        public int IngresarProducto(Producto producto)
        {

            using (var db = new BD_ProyectoValkyriaContext())
            {
                db.Productos.Add(producto);
               return db.SaveChanges();

            }

        }

        public List<Producto> CargarProductos()
        {
            List<Producto> lista = new List<Producto>();
            using (var db = new BD_ProyectoValkyriaContext())
            {
                    var resultado = (from pr in db.Productos
                                     select new
                                     {
                                         id = pr.IdProducto,
                                         nombre = pr.NombreProducto,
                                         descripcion = pr.Descripcion,
                                         tipoProducto = pr.TipoProducto,
                                         cantInventario = pr.CantInventario,
                                         precio = pr.Precio,
                                         idImagen = pr.IdImagen
                                     }
                                     ).ToList().GroupBy(x => x.id).Select(g => g.First());

                foreach (var item in resultado)
                {
                    lista.Add(new Producto
                    {
                        IdProducto = item.id,
                        NombreProducto = item.nombre,
                        Descripcion = item.descripcion,
                        TipoProducto = item.tipoProducto,
                        CantInventario = item.cantInventario,
                        Precio = item.precio,
                        IdImagen = item.idImagen

                    });
                }
            }

            return lista;
        }

        public Producto CargarProductoPorId(int idProducto)
        {
            using (var db = new BD_ProyectoValkyriaContext())
            {
                if (idProducto <= 0)
                    return null;

                var resultado = (from pr in db.Productos
                                 where pr.IdProducto == idProducto
                                 select new
                                 {
                                     id = pr.IdProducto,
                                     nombre = pr.NombreProducto,
                                     descripcion = pr.Descripcion,
                                     tipoProducto = pr.TipoProducto,
                                     cantInventario = pr.CantInventario,
                                     precio = pr.Precio,
                                     idImagen = pr.IdImagen
                                 }).FirstOrDefault();

                if (null != resultado)
                {
                    Producto prod = new Producto();
                    prod.NombreProducto = resultado.nombre;
                    prod.Descripcion = resultado.descripcion;
                    prod.TipoProducto = resultado.tipoProducto;
                    prod.CantInventario = resultado.cantInventario;
                    prod.Precio = resultado.precio;
                    prod.IdImagen = resultado.idImagen;

                    return prod;
                }

                return null;
            }

        }



        public ProductoImagen CargarProductoImagenPorId(int idProducto)
        {
            using (var db = new BD_ProyectoValkyriaContext())
            {
                if (idProducto <= 0)
                    return null;

                var resultado = (from pr in db.Productos
                                 join img in db.Imagens on pr.IdImagen equals img.IdImagen
                                 where pr.IdProducto == idProducto
                                 select new
                                 {
                                     id = pr.IdProducto,
                                     nombre = pr.NombreProducto,
                                     descripcion = pr.Descripcion,
                                     tipoProducto = pr.TipoProducto,
                                     cantInventario = pr.CantInventario,
                                     precio = pr.Precio,
                                     idImagen = pr.IdImagen,
                                     datosImagen = img.DatosImagen
                                 }).FirstOrDefault();

                if (null != resultado)
                {
                    ProductoImagen prod = new ProductoImagen();
                    prod.IdProducto = resultado.id;
                    prod.NombreProducto = resultado.nombre;
                    prod.Descripcion = resultado.descripcion;
                    prod.TipoProducto = resultado.tipoProducto;
                    prod.CantInventario = resultado.cantInventario;
                    prod.Precio = resultado.precio;
                    prod.IdImagen = resultado.idImagen;
                    prod.DatosImagen = resultado.datosImagen;
                    prod.imgDataURL = string.Format("data:image/png;base64,{0}", Convert.ToBase64String(resultado.datosImagen));

                    return prod;
                }

                return null;
            }

        }

        public List<ProductoImagen> CargarProductosConImagen()
        {
            List<ProductoImagen> lista = new List<ProductoImagen>();
            using (var db = new BD_ProyectoValkyriaContext())
            {
                var resultado = (from pr in db.Productos 
                                 join img in db.Imagens on pr.IdImagen equals img.IdImagen
                                 select new
                                 {
                                     id = pr.IdProducto,
                                     nombre = pr.NombreProducto,
                                     descripcion = pr.Descripcion,
                                     tipoProducto = pr.TipoProducto,
                                     cantInventario = pr.CantInventario,
                                     precio = pr.Precio,
                                     idImagen = pr.IdImagen,
                                     datosImagen = img.DatosImagen

                                 }
                                 ).ToList().GroupBy(x => x.id).Select(g => g.First());

                foreach (var item in resultado)
                {

                    lista.Add(new ProductoImagen
                    {
                        IdProducto = item.id,
                        NombreProducto = item.nombre,
                        Descripcion = item.descripcion,
                        TipoProducto = item.tipoProducto,
                        CantInventario = item.cantInventario,
                        Precio = item.precio,
                        IdImagen = item.idImagen,
                        DatosImagen = item.datosImagen,
                        imgDataURL = string.Format("data:image/png;base64,{0}", Convert.ToBase64String(item.datosImagen))


                    });
                }
            }

            return lista;
        }

        public int ModificarProducto(Producto producto)
        {
            using (var db = new BD_ProyectoValkyriaContext()) {

                if (null == producto || producto.IdProducto<=0)
                    return 0;

                var resultado = (from p in db.Productos
                                 where p.IdProducto == producto.IdProducto
                                 select p).FirstOrDefault();

                if (null != resultado)
                {
                    resultado.NombreProducto = producto.NombreProducto;
                    resultado.Descripcion = producto.Descripcion;
                    resultado.TipoProducto = producto.TipoProducto;
                    resultado.CantInventario = producto.CantInventario;
                    resultado.Precio = producto.Precio;
                    resultado.IdImagen = producto.IdImagen;
                    return db.SaveChanges();
                }

                return 0;

            }
        }

        public int EliminarProducto(int idProducto)
        {
            using (var db = new BD_ProyectoValkyriaContext())
            {
                if (idProducto <= 0)
                    return 0;

                var resultado = (from p in db.Productos
                                 where p.IdProducto == idProducto
                                 select p).FirstOrDefault();

                if (null != resultado)
                {
                    db.Productos.Remove(resultado);
                    return db.SaveChanges();
                }

                return 0;
            }

        }


        /*------------- REPORTES -----------------*/


        public decimal totalVentasSemana()
        {
            DateTime date =  DateTime.Now;
            DateTime mondayOfLastWeek = date.AddDays(-(int)date.DayOfWeek - 6);

            using (var db = new BD_ProyectoValkyriaContext())
            {
                var sums = from x in db.FacturaDetalles
                           join z in db.Facturas on x.IdFactura equals z.IdFactura
                           join y in db.Productos on x.IdProducto equals y.IdProducto
                           where z.Fecha >= mondayOfLastWeek
                           group x by new { y.TipoProducto, x.Precio } into g
                           select new
                           {
                               Precio = g.Sum(x => x.Precio)
                           };
                var result = sums.FirstOrDefault();

                if (null != result)
                {
                    return (decimal)result.Precio;
                }
                else
                {
                    return 0;
                }
               

            }
        }

        public decimal totalVentasProductosDeportivos()
        {

            using (var db = new BD_ProyectoValkyriaContext())
            {
                var sums = from x in db.FacturaDetalles
                           join y in db.Productos on x.IdProducto equals y.IdProducto
                           where y.TipoProducto=="Ropa Deportiva"
                           group x by new { y.TipoProducto, x.Precio } into g
                           select new
                           {
                               Precio = g.Sum(x => x.Precio)
                           };

                var result = sums.FirstOrDefault();

                if (null != result)
                {
                    return (decimal)result.Precio;
                }
                else
                {
                    return 0;
                }
                
                

            }
        }


        public decimal totalVentasSuplementos()
        {

            using (var db = new BD_ProyectoValkyriaContext())
            {
                var sums = from x in db.FacturaDetalles
                           join y in db.Productos on x.IdProducto equals y.IdProducto
                           where y.TipoProducto == "Suplementos"
                           group x by new { y.TipoProducto, x.Precio } into g
                           select new
                           {
                               Precio = g.Sum(x => x.Precio)
                           };
                var result = sums.FirstOrDefault();

                if (result != null)
                {
                    return (decimal)result.Precio;
                }
                else
                {
                    return 0;
                }
               

            }
        }


        public decimal totalVentasAccesorios()
        {

            using (var db = new BD_ProyectoValkyriaContext())
            {
                var sums = from x in db.FacturaDetalles
                           join y in db.Productos on x.IdProducto equals y.IdProducto
                           where y.TipoProducto == "Accesorios"
                           group x by new { y.TipoProducto, x.Precio } into g
                           select new
                           {
                               Precio = g.Sum(x => x.Precio)
                           };
                var result = sums.FirstOrDefault();

                if (null != result) {
                    return (decimal)result.Precio;
                }
                else
                {
                    return 0;
                }

                

            }
        }


        public decimal totalVentas()
        {

            using (var db = new BD_ProyectoValkyriaContext())
            {
                var sums = from x in db.FacturaDetalles
                           join y in db.Productos on x.IdProducto equals y.IdProducto
                           group x by new { y.TipoProducto, x.Precio } into g
                           select new
                           {
                               Precio = g.Sum(x => x.Precio)
                           };
                var result = sums.FirstOrDefault();

                if (null != result)
                {

                    return (decimal)result.Precio;
                }
                else
                {
                    return 0;
                }


            }
        }

        public int cantProductos()
        {

            using (var db = new BD_ProyectoValkyriaContext())
            {
                var cant = (from x in db.Productos
                            select x).Count();

                if (cant != null)
                {
                    return (int)cant;
                }
                else
                {
                    return 0;
                }

            }
        }

        public int totalOrdenes()
        {

            using (var db = new BD_ProyectoValkyriaContext())
            {
                var cant = (from x in db.FacturaDetalles
                            join y in db.Productos on x.IdProducto equals y.IdProducto
                            select x).Count();

                if (cant != null)
                {
                    return (int)cant;
                }
                else
                {
                    return 0;
                }

            }
        }







    }
}
