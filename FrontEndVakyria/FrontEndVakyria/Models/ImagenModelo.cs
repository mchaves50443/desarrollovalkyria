﻿using FrontEndVakyria.Modelos_BD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEndVakyria.Models
{
    public class ImagenModelo
    {

        public int IngresarImagen(Imagen imagen)
        {

            using (var db = new BD_ProyectoValkyriaContext())
            {
                db.Imagens.Add(imagen);
                return db.SaveChanges();
            }

        }


        public List<Imagen> CargarImagenes()
        {
            List<Imagen> lista = new List<Imagen>();
            using (var db = new BD_ProyectoValkyriaContext())
            {
                var resultado = (from img in db.Imagens
                                 select new
                                 {
                                     id = img.IdImagen,
                                     nombre = img.NombreImagen,
                                     datosImagen = img.DatosImagen,
                                 }
                                 ).ToList().GroupBy(x => x.id).Select(g => g.First());

                foreach (var item in resultado)
                {
                    lista.Add(new Imagen
                    {
                        IdImagen = item.id,
                        NombreImagen = item.nombre,
                        DatosImagen = item.datosImagen

                    });
                }
            }

            return lista;
        }



        public int ModificarImagen(Imagen imagen)
        {
            using (var db = new BD_ProyectoValkyriaContext())
            {

                if (null == imagen || imagen.IdImagen <= 0)
                    return 0;

                var resultado = (from p in db.Imagens
                                 where p.IdImagen == imagen.IdImagen
                                 select p).FirstOrDefault();

                if (null != resultado)
                {
                    resultado.NombreImagen = imagen.NombreImagen;
                    resultado.DatosImagen = imagen.DatosImagen;
                    return db.SaveChanges();
                }

                return 0;

            }
        }

        public int EliminarImagen(int idImagen)
        {

            List<Producto> listaProductos = new List<Producto>();
            List<Workshop> listaWorkshops = new List<Workshop>();

            using (var db = new BD_ProyectoValkyriaContext())
            {
                if (idImagen <= 0)
                    return 0;


                //Eliminar todos los idImagen de los productos que lo tengan
                var resultadoProducto = (from pr in db.Productos
                                 where pr.IdImagen == idImagen
                                 select new 
                                 {
                                     id = pr.IdProducto,
                                     nombre = pr.NombreProducto,
                                     descripcion = pr.Descripcion,
                                     tipoProducto = pr.TipoProducto,
                                     cantInventario = pr.CantInventario,
                                     precio = pr.Precio,
                                     idImagen = pr.IdImagen
                                 }
                                 ).ToList().GroupBy(x => x.id).Select(g => g.First());


                foreach (var item in resultadoProducto)
                {
                    listaProductos.Add(new Producto
                    {
                        IdProducto = item.id,
                        NombreProducto = item.nombre,
                        Descripcion = item.descripcion,
                        TipoProducto = item.tipoProducto,
                        CantInventario = item.cantInventario,
                        Precio = item.precio,
                        IdImagen = item.idImagen

                    });
                }

                foreach (var item in listaProductos)
                {
                    if (null != item)
                    {
                        item.IdImagen = null;
                        db.SaveChanges();
                    }                   
                }



                var resultadoWorkshops = (from ws in db.Workshops
                                         where ws.IdImagen == idImagen
                                         select new
                                         {
                                             id = ws.IdWorkshop,
                                             nombre = ws.NombreWorkshop,
                                             descripcion = ws.Descripcion,
                                             minInventario = ws.MinInventario,
                                             maxInventario = ws.MaxInventatio,
                                             Precio = ws.Precio,
                                             Fecha = ws.Fecha,
                                             idImagen = ws.IdImagen,
                                         }).ToList().GroupBy(x => x.id).Select(g => g.First());


                foreach (var item in resultadoWorkshops)
                {
                    listaWorkshops.Add(new Workshop
                    {
                        IdWorkshop = item.id,
                        NombreWorkshop = item.nombre,
                        Descripcion = item.descripcion,
                        MinInventario = item.minInventario,
                        MaxInventatio = item.maxInventario,
                        Precio = item.Precio,
                        Fecha = item.Fecha,
                        IdImagen = item.idImagen
                    });
                }

                foreach (var item in listaWorkshops)
                {
                    if (null != item)
                    {
                        item.IdImagen = null;
                        db.SaveChanges();
                    }
                }


                //Eliminamos imagen
                var resultado = (from p in db.Imagens
                                 where p.IdImagen == idImagen
                                 select p).FirstOrDefault();

                if (null != resultado)
                {
                    db.Imagens.Remove(resultado);
                    return db.SaveChanges();
                }

                return 0;
            }
        }


    }
}
