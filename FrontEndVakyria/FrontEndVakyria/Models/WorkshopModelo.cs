﻿using FrontEndVakyria.Entities;
using FrontEndVakyria.Modelos_BD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEndVakyria.Models
{
    public class WorkshopModelo
    {

        public int IngresarWorkshop(Workshop workshop)
        {

            using (var db = new BD_ProyectoValkyriaContext())
            {
                db.Workshops.Add(workshop);
                return db.SaveChanges();

            }

        }

        public List<Workshop> CargarWorkshop()
        {
            List<Workshop> lista = new List<Workshop>();
            using (var db = new BD_ProyectoValkyriaContext())
            {
                var resultado = (from pr in db.Workshops
                                 select new
                                 {
                                     id = pr.IdWorkshop,
                                     nombre = pr.NombreWorkshop,
                                     descripcion = pr.Descripcion,
                                     minInventario = pr.MinInventario,
                                     maxInventario = pr.MaxInventatio,
                                     Precio = pr.Precio,
                                     Fecha = pr.Fecha,
                                     //idImagen = pr.IdImagen
                                 }
                                 ).ToList().GroupBy(x => x.id).Select(g => g.First());

                foreach (var item in resultado)
                {
                    lista.Add(new Workshop
                    {
                        IdWorkshop = item.id,
                        NombreWorkshop = item.nombre,
                        Descripcion = item.descripcion,
                        MinInventario = item.minInventario,
                        MaxInventatio = item.maxInventario,
                        Precio = item.Precio,
                        Fecha = item.Fecha
                        //IdImagen = item.idImagen

                    });
                }
            }

            return lista;
        }

        public Workshop CargarWorkshopPorId(int IdWorkshop)
        {
            using (var db = new BD_ProyectoValkyriaContext())
            {
                if (IdWorkshop <= 0)
                    return null;

                var resultado = (from pr in db.Workshops
                                 where pr.IdWorkshop == IdWorkshop
                                 select new
                                 {
                                     id = pr.IdWorkshop,
                                     nombre = pr.NombreWorkshop,
                                     descripcion = pr.Descripcion,
                                     minInventario = pr.MinInventario,
                                     maxInventario = pr.MaxInventatio,
                                     Precio = pr.Precio,
                                     Fecha = pr.Fecha,
                                     //IdImagen = item.idImagen
                                 }).FirstOrDefault();

                if (null != resultado)
                {
                    Workshop work = new Workshop();
                    work.IdWorkshop = resultado.id;
                    work.NombreWorkshop = resultado.nombre;
                    work.Descripcion = resultado.descripcion;
                    work.MinInventario = resultado.minInventario;
                    work.MaxInventatio = resultado.maxInventario;
                    work.Precio = resultado.Precio;
                    work.Fecha = resultado.Fecha;
                    //prod.IdImagen = resultado.idImagen;

                    return work;
                }

                return null;
            }

        }

        public List<WorkshopImagen> CargarWorkshopsConImagen()
        {
            List<WorkshopImagen> lista = new List<WorkshopImagen>();
            using (var db = new BD_ProyectoValkyriaContext())
            {
                var resultado = (from ws in db.Workshops
                                 join img in db.Imagens on ws.IdImagen equals img.IdImagen
                                 select new
                                 {
                                     id = ws.IdWorkshop,
                                     nombre = ws.NombreWorkshop,
                                     descripcion = ws.Descripcion,
                                     minInventario = ws.MinInventario,
                                     maxInventario = ws.MaxInventatio,
                                     Precio = ws.Precio,
                                     Fecha = ws.Fecha,
                                     idImagen = ws.IdImagen,
                                     datosImagen = img.DatosImagen

                                 }
                                 ).ToList().GroupBy(x => x.id).Select(g => g.First());

                foreach (var item in resultado)
                {

                    lista.Add(new WorkshopImagen
                    {
                        IdWorkshop = item.id,
                        NombreWorkshop = item.nombre,
                        Descripcion = item.descripcion,
                        MinInventario = item.minInventario,
                        MaxInventatio = item.maxInventario,
                        Precio = item.Precio,
                        Fecha = item.Fecha,
                        IdImagen = item.idImagen,
                        DatosImagen = item.datosImagen,
                        imgDataURL = string.Format("data:image/png;base64,{0}", Convert.ToBase64String(item.datosImagen))


                    });
                }
            }

            return lista;
        }


        public WorkshopImagen CargarWorkshopImagenPorId(int idWorkshop)
        {
            using (var db = new BD_ProyectoValkyriaContext())
            {
                if (idWorkshop <= 0)
                    return null;

                var resultado = (from ws in db.Workshops
                                 join img in db.Imagens on ws.IdImagen equals img.IdImagen
                                 where ws.IdWorkshop == idWorkshop
                                 select new
                                 {
                                     id = ws.IdWorkshop,
                                     nombre = ws.NombreWorkshop,
                                     descripcion = ws.Descripcion,
                                     minInventario = ws.MinInventario,
                                     maxInventario = ws.MaxInventatio,
                                     Precio = ws.Precio,
                                     Fecha = ws.Fecha,
                                     idImagen = ws.IdImagen,
                                     datosImagen = img.DatosImagen


                                 }).FirstOrDefault();

                if (null != resultado)
                {
                    WorkshopImagen work = new WorkshopImagen();
                    work.IdWorkshop = resultado.id;
                    work.NombreWorkshop = resultado.nombre;
                    work.Descripcion = resultado.descripcion;
                    work.MinInventario = resultado.minInventario;
                    work.MaxInventatio = resultado.maxInventario;
                    work.Precio = resultado.Precio;
                    work.Fecha = resultado.Fecha;
                    work.IdImagen = resultado.idImagen;
                    work.DatosImagen = resultado.datosImagen;
                    work.imgDataURL = string.Format("data:image/png;base64,{0}", Convert.ToBase64String(resultado.datosImagen));

                    return work;
                }

                return null;
            }

        }

        public int ModificarWorkshop(Workshop workshop)
        {
            using (var db = new BD_ProyectoValkyriaContext())
            {

                if (null == workshop || workshop.IdWorkshop <= 0)
                    return 0;

                var resultado = (from p in db.Workshops
                                 where p.IdWorkshop == workshop.IdWorkshop
                                 select p).FirstOrDefault();

                if (null != resultado)
                {
                    resultado.NombreWorkshop = workshop.NombreWorkshop;
                    resultado.Descripcion = workshop.Descripcion;
                    resultado.MinInventario = workshop.MinInventario;
                    resultado.MaxInventatio = workshop.MaxInventatio;
                    resultado.Precio = workshop.Precio;
                    resultado.Fecha = workshop.Fecha;
                    //resultado.IdImagen = Workshop.IdImagen;
                    return db.SaveChanges();
                }

                return 0;

            }
        }

        public int EliminarWorkshop(int idWorkshop)
        {
            using (var db = new BD_ProyectoValkyriaContext())
            {
                if (idWorkshop <= 0)
                    return 0;

                var resultado = (from p in db.Workshops
                                 where p.IdWorkshop == idWorkshop
                                 select p).FirstOrDefault();

                if (null != resultado)
                {
                    db.Workshops.Remove(resultado);
                    return db.SaveChanges();
                }

                return 0;
            }

        }

        public decimal totalWorkshops()
        {

            using (var db = new BD_ProyectoValkyriaContext())
            {
                var sums = from x in db.FacturaDetalles
                           where x.IdWorkshop != null
                           select new
                           {
                               Precio = db.FacturaDetalles.Where(n => n.IdWorkshop != null).Sum(n => n.Precio)
                           };
                var result = sums.FirstOrDefault();
                return (decimal)result.Precio;

            }
        }

        //public decimal totalWorkshops()
        //{

        //    using (var db = new BD_ProyectoValkyriaContext())
        //    {
        //        var sums = from x in db.FacturaDetalles
        //                   join y in db.Workshops on x.IdWorkshop equals y.IdWorkshop
        //                   group x by new { y.IdWorkshop, x.Precio } into g
        //                   select new
        //                   {
        //                       Precio = g.Sum(x => x.Precio)
        //                   };
        //        var result = sums.FirstOrDefault();
        //        return (decimal)result.Precio;

        //    }
        //}

        public decimal totalEventos()
        {

            using (var db = new BD_ProyectoValkyriaContext())
            {
                var sums = from x in db.FacturaDetalles
                           join y in db.Workshops on x.IdWorkshop equals y.IdWorkshop
                           group x by new {x.Precio } into g
                           select new
                           {
                               Precio = g.Sum(x => x.Precio)
                           };
                var result = sums.FirstOrDefault();
                return (decimal)result.Precio;

            }
        }

        public int cuposDisponibles()
        {

            using (var db = new BD_ProyectoValkyriaContext())
            {
                var sums = from x in db.Workshops
                           select new
                           {
                               Cupos = db.Workshops.Sum(n => n.MaxInventatio),
                               Vendidos = db.Workshops.Sum(n => n.CuposVendidos)
                           };
                var result = sums.FirstOrDefault();
                var total = result.Cupos - result.Vendidos;
                if (result != null)
                {
                    return (int)total;
                }
                else
                {
                    return 0;
                }

            }
        }

        public int cuposVendidos()
        {

            using (var db = new BD_ProyectoValkyriaContext())
            {
                var sums = from x in db.Workshops
                           select new
                           {
                               Vendidos = db.Workshops.Sum(n => n.CuposVendidos)
                           };
                var result = sums.FirstOrDefault();
                return (int)result.Vendidos;

            }
        }

        public int cuposWorkshops()
        {

            using (var db = new BD_ProyectoValkyriaContext())
            {
                var sums = from x in db.Workshops
                          
                           select new
                           {
                               Total = db.Workshops.Sum(n => n.CuposVendidos)
                           };
                var result = sums.FirstOrDefault();
                return (int)result.Total;

            }
        }

        public int cuposEventos()
        {

            using (var db = new BD_ProyectoValkyriaContext())
            {
                var sums = from x in db.Workshops
                           select new
                           {
                               Total = db.Workshops.Sum(n => n.CuposVendidos)
                           };
                var result = sums.FirstOrDefault();

                if (result != null)
                {
                    return (int)result.Total;
                }
                else
                {
                    return 0;
                }
            }
        }


    }
}

