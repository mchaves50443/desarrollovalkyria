﻿using FrontEndVakyria.Entities;
using FrontEndVakyria.Modelos_BD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEndVakyria.Models
{
    public class OrdenModelo
    {

        public List<Orden> CargarOrdenesPorId(long id)
        {
                List<Orden> lista = new List<Orden>();
                using (var db = new BD_ProyectoValkyriaContext())
                {
                    var primerResultado = (from or in db.Facturas
                                           join det in db.FacturaDetalles on or.IdFactura equals det.IdFactura
                                           where det.IdProducto != null
                                           && or.IdFactura==id
                                           select new
                                           {
                                               id = or.IdFactura,
                                               fechaOrden = or.Fecha,
                                               correo = or.Correo,
                                               telefono = or.Telefono,
                                               total = or.Total,
                                               estado = or.Estado,
                                               direccion = or.Direccion + ", " + or.Distrito + ", " + or.Canton + ", " + or.Provincia + ", " + or.CodigoPostal
                                           }
                                     ).ToList().GroupBy(x => x.id).Select(g => g.First());

                    foreach (var item in primerResultado)
                    {
                        lista.Add(new Orden
                        {
                            IdOrden = item.id,
                            fechaOrden = item.fechaOrden,
                            correoElectronico = item.correo,
                            telefono = item.telefono,
                            total = item.total,
                            estado = item.estado,
                            direccionCompleta = item.direccion
                        });
                    }
                }

                foreach (Orden item in lista)
                {
                    item.listaItems = CargarItems(item.IdOrden);
                    item.cantidad = item.listaItems.Count();
                };

                return lista;
        }


        public List<Orden> CargarOrdenesConRango(DateTime inicio, DateTime final, byte estado)
        {
            List<Orden> lista = new List<Orden>();
            using (var db = new BD_ProyectoValkyriaContext())
            {
                var primerResultado = (from or in db.Facturas
                                       join det in db.FacturaDetalles on or.IdFactura equals det.IdFactura
                                       where det.IdProducto != null
                                       && or.Fecha >= inicio && or.Fecha<=final
                                       && or.Estado==estado
                                       select new
                                       {
                                           id = or.IdFactura,
                                           fechaOrden = or.Fecha,
                                           correo = or.Correo,
                                           telefono = or.Telefono,
                                           total = or.Total,
                                           estado = or.Estado,
                                           direccion = or.Direccion + ", " + or.Distrito + ", " + or.Canton + ", " + or.Provincia + ", " + or.CodigoPostal
                                       }
                                 ).ToList().GroupBy(x => x.id).Select(g => g.First());

                foreach (var item in primerResultado)
                {
                    lista.Add(new Orden
                    {
                        IdOrden = item.id,
                        fechaOrden = item.fechaOrden,
                        correoElectronico = item.correo,
                        telefono = item.telefono,
                        total = item.total,
                        estado = item.estado,
                        direccionCompleta = item.direccion
                    });
                }
            }

            foreach (Orden item in lista)
            {
                item.listaItems = CargarItems(item.IdOrden);
                item.cantidad = item.listaItems.Count();
            };

            return lista;
        }

        public List<Item> CargarItems(long idFactura)
        {
            List<Item> lista = new List<Item>();
            using (var db = new BD_ProyectoValkyriaContext())
            {
                var primerResultado = (from pr in db.Productos
                                       join det in db.FacturaDetalles on pr.IdProducto equals det.IdProducto
                                       where det.IdFactura == idFactura
                                       select new
                                       {
                                           id = pr.IdProducto,
                                           nombre = pr.NombreProducto,
                                           tipoItem = pr.TipoProducto,
                                           precio = det.Precio,
                                           cantidad = det.Cantidad
                                       }
                                 ).ToList();

                foreach (var item in primerResultado)
                {
                    lista.Add(new Item
                    {
                        IdItem = item.id,
                        Nombre = item.nombre,
                        TipoItem = item.tipoItem.ToString(),
                        Precio = item.precio.Value,
                        Cantidad = item.cantidad.Value

                    });
                }
            }

            return lista;
        }



        public int ModificarOrden(long idOrden, byte estado)
        {
            using (var db = new BD_ProyectoValkyriaContext())
            {

                if (null == idOrden || idOrden <= 0)
                    return 0;

                var resultado = (from ac in db.Facturas
                                 where ac.IdFactura == idOrden
                                 select ac).FirstOrDefault();

                if (null != resultado)
                {
                    resultado.Estado = estado;
                    return db.SaveChanges();
                }

                return 0;

            }
        }





    }

}
