﻿using FrontEndVakyria.Modelos_BD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEndVakyria.Models
{
    public class ConsultaModelo
    {
        internal void IngresarConsulta(Consulta consulta)
        {
            using (var db = new BD_ProyectoValkyriaContext())
            {
                db.Consultas.Add(consulta);
                db.SaveChanges();

            }
        }

        public List<Consulta> CargarConsultas()
        {
            List<Consulta> lista = new List<Consulta>();
            using (var db = new BD_ProyectoValkyriaContext())
            {
                var resultado = from c in db.Consultas
                                orderby c descending
                                select c;

                foreach (var item in resultado)
                {
                    lista.Add(new Consulta
                    {
                        IdConsulta = item.IdConsulta,
                        NombrePersona = item.NombrePersona,
                        Mensaje = item.Mensaje,
                        Email = item.Email,
                        Consulta1 = item.Consulta1,
                        FechaIngresada = item.FechaIngresada

                    });
                }
            }

            return lista;
        }

        public int EliminarConsulta(int idCon)
        {
            using (var db = new BD_ProyectoValkyriaContext())
            {
                if (idCon <= 0)
                    return 0;

                var resultado = (from c in db.Consultas
                                 where c.IdConsulta == idCon
                                 select c).FirstOrDefault();

                if (null != resultado)
                {
                    db.Consultas.Remove(resultado);
                    return db.SaveChanges();
                }

                return 0;
            }
        }
    }
}
