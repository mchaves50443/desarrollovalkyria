﻿using FrontEndVakyria.Modelos_BD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontEndVakyria.Models
{
    public class LoginModelo
    {


        private const string Key = "adef@@kfxcbv@";

        internal void RegistrarUsuario(Usuario usuario)
        {
            using (var db = new BD_ProyectoValkyriaContext())
            {
                usuario.RolUsuario = 333;
                usuario.ContrasenaUsuario = Encriptar(usuario.ContrasenaUsuario);
                db.Usuarios.Add(usuario);
                db.SaveChanges();

            }
        }

        internal void ActualizarContrasenaUsuario(Usuario usuario)
        {
            using (var db = new BD_ProyectoValkyriaContext())
            {

                var user = (from x in db.Usuarios
                            where x.IdUsuario == usuario.IdUsuario
                            select x).FirstOrDefault();

                if (null != user)
                {
                    user.ContrasenaUsuario = Encriptar(usuario.ContrasenaUsuario);
                    db.SaveChanges();
                }

            }
        }

        public int totalUsuarios()
        {
            using (var db = new BD_ProyectoValkyriaContext())
            {
                var sums = from x in db.Usuarios
                           select new
                           {
                               Cantidad = db.Usuarios.Where(n => n.RolUsuario == 333).Count()
                           };
                var result = sums.FirstOrDefault();
                return result.Cantidad;

            }
        }

        public Usuario ValidarLogin(Usuario user)
        {
            string cont = Encriptar(user.ContrasenaUsuario);
            using (var bd = new BD_ProyectoValkyriaContext())
            {
                var resultado = (from x in bd.Usuarios
                                 where x.CorreoElectronico == user.CorreoElectronico && x.ContrasenaUsuario == cont
                                 select new
                                 {
                                     id = x.IdUsuario,
                                     nombre = x.NombreUsuario,
                                     primerapellido = x.PrimerApellidoUsuario,
                                     segundoapellido = x.SegundoApellidoUsuario,
                                     cedula = x.CedulaUsuario,
                                     correo = x.CorreoElectronico,
                                     telefono = x.TelefonoUsuario,
                                     contrasena = x.ContrasenaUsuario,
                                     rol = x.RolUsuario,
                                     ultimaconn = x.UltimaConexion
                                 }).FirstOrDefault();
                if (null != resultado)
                {
                    Usuario usuario = new Usuario();
                    usuario.IdUsuario = resultado.id;
                    usuario.NombreUsuario = resultado.nombre;
                    usuario.PrimerApellidoUsuario = resultado.primerapellido;
                    usuario.SegundoApellidoUsuario = resultado.segundoapellido;
                    usuario.CedulaUsuario = resultado.cedula;
                    usuario.CorreoElectronico = resultado.correo;
                    usuario.TelefonoUsuario = resultado.telefono;
                    usuario.ContrasenaUsuario = Desencriptar(resultado.contrasena);
                    usuario.RolUsuario = resultado.rol;
                    usuario.UltimaConexion = resultado.ultimaconn;

                    return usuario;
                }

                return null;
            }
        }

        internal object ValidarRegistro(string correoElectronico)
        {
            using (var bd = new BD_ProyectoValkyriaContext())
            {
                var resultado = (from x in bd.Usuarios
                                 where x.CorreoElectronico == correoElectronico
                                 select x).FirstOrDefault();

                return resultado;

            }
        }

        public  Usuario ConseguirUsuarioConCedula(string cedula)
        {
            using (var bd = new BD_ProyectoValkyriaContext())
            {
                var resultado = (from x in bd.Usuarios
                                 where x.CedulaUsuario == cedula
                                 select x).FirstOrDefault();

                return resultado;

            }

            return null;
        }


        public static string Encriptar(string password)
        {
            if (string.IsNullOrEmpty(password)) return "";
            password += Key;
            var passwordBytes = Encoding.UTF8.GetBytes(password);
            return Convert.ToBase64String(passwordBytes);
        }

        public static string Desencriptar(string base64EncodeData)
        {
            if (string.IsNullOrEmpty(base64EncodeData)) return "";
            var base64EncodeBytes = Convert.FromBase64String(base64EncodeData);
            var result = Encoding.UTF8.GetString(base64EncodeBytes);
            result = result.Substring(0, result.Length - Key.Length);
            return result;
        }
    }
}
