﻿using FrontEndVakyria.Entities;
using FrontEndVakyria.Modelos_BD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEndVakyria.Models
{
    public class ProgramaModelo
    {
        public int IngresarPrograma(Programa Programa)
        {

            using (var db = new BD_ProyectoValkyriaContext())
            {
                db.Programas.Add(Programa);
                return db.SaveChanges();

            }

        }

        public List<Programa> CargarProgramas()
        {
            List<Programa> lista = new List<Programa>();
            using (var db = new BD_ProyectoValkyriaContext())
            {
                var resultado = (from pr in db.Programas
                                 select new
                                 {
                                     id = pr.IdPrograma,
                                     nombre = pr.NombrePrograma,
                                     descripcion = pr.Descripcion,
                                     precio = pr.Precio,
                                     idImagen = pr.IdImagen
                                 }
                                 ).ToList().GroupBy(x => x.id).Select(g => g.First());

                foreach (var item in resultado)
                {
                    lista.Add(new Programa
                    {
                        IdPrograma = item.id,
                        NombrePrograma = item.nombre,
                        Descripcion = item.descripcion,
                        Precio = item.precio,
                        IdImagen = item.idImagen

                    });
                }
            }

            return lista;
        }

        public Programa CargarProgramaPorId(int idPrograma)
        {
            using (var db = new BD_ProyectoValkyriaContext())
            {
                if (idPrograma <= 0)
                    return null;

                var resultado = (from pr in db.Programas
                                 where pr.IdPrograma == idPrograma
                                 select new
                                 {
                                     id = pr.IdPrograma,
                                     nombre = pr.NombrePrograma,
                                     descripcion = pr.Descripcion,
                                     precio = pr.Precio,
                                     idImagen = pr.IdImagen
                                 }).FirstOrDefault();

                if (null != resultado)
                {
                    Programa prod = new Programa();
                    prod.NombrePrograma = resultado.nombre;
                    prod.Descripcion = resultado.descripcion;
                    prod.Precio = resultado.precio;
                    prod.IdImagen = resultado.idImagen;

                    return prod;
                }

                return null;
            }

        }




        public ProgramaImagen CargarProgramaImagenPorId(int idPrograma)
        {
            using (var db = new BD_ProyectoValkyriaContext())
            {
                if (idPrograma <= 0)
                    return null;

                var resultado = (from pr in db.Programas
                                 join img in db.Imagens on pr.IdImagen equals img.IdImagen
                                 where pr.IdPrograma == idPrograma
                                 select new
                                 {
                                     id = pr.IdPrograma,
                                     nombre = pr.NombrePrograma,
                                     descripcion = pr.Descripcion,
                                     precio = pr.Precio,
                                     idImagen = pr.IdImagen,
                                     datosImagen = img.DatosImagen
                                 }).FirstOrDefault();

                if (null != resultado)
                {
                    ProgramaImagen prod = new ProgramaImagen();
                    prod.IdPrograma = resultado.id;
                    prod.NombrePrograma = resultado.nombre;
                    prod.Descripcion = resultado.descripcion;
                    prod.Precio = resultado.precio;
                    prod.IdImagen = resultado.idImagen;
                    prod.DatosImagen = resultado.datosImagen;
                    prod.imgDataURL = string.Format("data:image/png;base64,{0}", Convert.ToBase64String(resultado.datosImagen));

                    return prod;
                }

                return null;
            }

        }

        public List<ProgramaImagen> CargarProgramasConImagen()
        {
            List<ProgramaImagen> lista = new List<ProgramaImagen>();
            using (var db = new BD_ProyectoValkyriaContext())
            {
                var resultado = (from pr in db.Programas
                                 join img in db.Imagens on pr.IdImagen equals img.IdImagen
                                 select new
                                 {
                                     id = pr.IdPrograma,
                                     nombre = pr.NombrePrograma,
                                     descripcion = pr.Descripcion,
                                     precio = pr.Precio,
                                     idImagen = pr.IdImagen,
                                     datosImagen = img.DatosImagen

                                 }
                                 ).ToList().GroupBy(x => x.id).Select(g => g.First());

                foreach (var item in resultado)
                {

                    lista.Add(new ProgramaImagen
                    {
                        IdPrograma = item.id,
                        NombrePrograma = item.nombre,
                        Descripcion = item.descripcion,
                        Precio = item.precio,
                        IdImagen = item.idImagen,
                        DatosImagen = item.datosImagen,
                        imgDataURL = string.Format("data:image/png;base64,{0}", Convert.ToBase64String(item.datosImagen))


                    });
                }
            }

            return lista;
        }

        public int ModificarPrograma(Programa Programa)
        {
            using (var db = new BD_ProyectoValkyriaContext())
            {

                if (null == Programa || Programa.IdPrograma <= 0)
                    return 0;

                var resultado = (from p in db.Programas
                                 where p.IdPrograma == Programa.IdPrograma
                                 select p).FirstOrDefault();

                if (null != resultado)
                {
                    resultado.NombrePrograma = Programa.NombrePrograma;
                    resultado.Descripcion = Programa.Descripcion;
                    resultado.Precio = Programa.Precio;
                    resultado.IdImagen = Programa.IdImagen;
                    return db.SaveChanges();
                }

                return 0;

            }
        }

        public int EliminarPrograma(int idPrograma)
        {
            using (var db = new BD_ProyectoValkyriaContext())
            {
                if (idPrograma <= 0)
                    return 0;

                var resultado = (from p in db.Programas
                                 where p.IdPrograma == idPrograma
                                 select p).FirstOrDefault();

                if (null != resultado)
                {
                    db.Programas.Remove(resultado);
                    return db.SaveChanges();
                }

                return 0;
            }

        }

        public decimal totalVentasSemana()
        {
            DateTime date = DateTime.Now;
            DateTime mondayOfLastWeek = date.AddDays(-(int)date.DayOfWeek - 6);

            using (var db = new BD_ProyectoValkyriaContext())
            {
                var sums = from x in db.FacturaDetalles
                           join z in db.Facturas on x.IdFactura equals z.IdFactura
                           join y in db.Programas on x.IdPrograma equals y.IdPrograma
                           where z.Fecha >= mondayOfLastWeek
                           group x by new { x.Precio } into g
                           select new
                           {
                               Precio = g.Sum(x => x.Precio)
                           };
                var result = sums.FirstOrDefault();

                if (null != result)
                {
                    return (decimal)result.Precio;
                }
                else
                {
                    return 0;
                }


            }
        }

        public decimal totalVentas()
        {

            using (var db = new BD_ProyectoValkyriaContext())
            {
                var sums = from x in db.FacturaDetalles
                           join y in db.Programas on x.IdPrograma equals y.IdPrograma
                           group x by new { x.Precio } into g
                           select new
                           {
                               Precio = g.Sum(x => x.Precio)
                           };
                var result = sums.FirstOrDefault();

                if (null != result)
                {

                    return (decimal)result.Precio;
                }
                else
                {
                    return 0;
                }


            }
        }

        public int cantProgramas()
        {

            using (var db = new BD_ProyectoValkyriaContext())
            {
                var cant = (from x in db.Programas
                            select x).Count();

                if (cant != null)
                {
                    return (int)cant;
                }
                else
                {
                    return 0;
                }

            }
        }
    }
}
