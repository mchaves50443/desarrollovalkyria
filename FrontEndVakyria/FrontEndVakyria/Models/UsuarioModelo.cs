﻿using FrontEndVakyria.Entities;
using FrontEndVakyria.Modelos_BD;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontEndVakyria.Models
{
    public class UsuarioModelo
    {
        private const string Key = "adef@@kfxcbv@";
        public int ModificarUsuario(Usuario usuario)
        {
            using (var db = new BD_ProyectoValkyriaContext())
            {

                var resultado = (from p in db.Usuarios
                                 where p.IdUsuario == usuario.IdUsuario
                                 select p).FirstOrDefault();

                if (null != resultado)
                {
                    resultado.NombreUsuario = usuario.NombreUsuario;
                    resultado.PrimerApellidoUsuario = usuario.PrimerApellidoUsuario;
                    resultado.SegundoApellidoUsuario = usuario.SegundoApellidoUsuario;
                    resultado.CedulaUsuario = usuario.CedulaUsuario;
                    resultado.CorreoElectronico = usuario.CorreoElectronico;
                    resultado.TelefonoUsuario = usuario.TelefonoUsuario;
                    resultado.ContrasenaUsuario = Encriptar(usuario.ContrasenaUsuario);
                    resultado.RolUsuario = 333;
                    return db.SaveChanges();
                }

                return 0; 

            }
        }

        public static string Encriptar(string password)
        {
            if (string.IsNullOrEmpty(password)) return "";
            password += Key;
            var passwordBytes = Encoding.UTF8.GetBytes(password);
            return Convert.ToBase64String(passwordBytes);
        }
    }
}
