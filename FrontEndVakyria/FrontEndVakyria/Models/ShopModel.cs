﻿using FrontEndVakyria.Entities;
using FrontEndVakyria.Modelos_BD;
using gpayments;
using gpayments.Model;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEndVakyria.Models
{
    public class ShopModel
    {
        internal void RegistrarFactura(Factura factura)
        {
            using (var db = new BD_ProyectoValkyriaContext())
            {
                factura.Total = 000;
                factura.Fecha = DateTime.Now;
                db.Facturas.Add(factura);
                db.SaveChanges();
            }
        }

        internal decimal Gananciastotales()
        {
            using (var db = new BD_ProyectoValkyriaContext())
            {
                var sums = from x in db.Facturas
                           select new
                           {
                               Precio = db.Facturas.Sum(n => n.Total)
                           };
                var result = sums.FirstOrDefault();
                return (decimal)result.Precio;

            }
        }

        internal int OrdenesTotales()
        {
            using (var db = new BD_ProyectoValkyriaContext())
            {
                var sums = from x in db.Facturas
                           select new
                           {
                               Cantidad = db.Facturas.Count()
                           };
                var result = sums.FirstOrDefault();
                return result.Cantidad;

            }
        }

        public List<Factura> misCompras(string idUsuario)
        {
            int id = int.Parse(idUsuario);
            List<Factura> lista = new List<Factura>();
            using (var db = new BD_ProyectoValkyriaContext())
            {
                var resultado = (from x in db.Facturas
                                 where x.IdUsuario == id
                                 select new
                                 {
                                     id = x.IdFactura,
                                     usuario = x.NombreUsuario,
                                     fecha = x.Fecha,
                                     total = x.Total
                                 }
                                 ).ToList().GroupBy(x => x.id).Select(g => g.First());

                foreach (var item in resultado)
                {
                    lista.Add(new Factura
                    {
                        IdFactura = item.id,
                        NombreUsuario = item.usuario,
                        Fecha = item.fecha,
                        Total = item.total

                    });
                }
            }

            return lista;

        }

        internal int ProductosTotales()
        {
            using (var db = new BD_ProyectoValkyriaContext())
            {
                var sums = from x in db.FacturaDetalles
                           select new
                           {
                               CantidadProgramas = db.FacturaDetalles.Where(n => n.IdPrograma != null).Count(),
                               CantidadProductos = db.FacturaDetalles.Where(n => n.IdProducto != null).Count(),
                               CantidadWorkshops = db.FacturaDetalles.Where(n => n.IdWorkshop != null).Count(),
                               CantidadActividades = db.FacturaDetalles.Where(n => n.IdActividad != null).Count()
                           };
                var result = sums.FirstOrDefault();
                var total = result.CantidadActividades + result.CantidadProductos + result.CantidadProgramas + result.CantidadWorkshops;
                return total;

            }
        }

        public bool RegistrarPago(SimpleCharge simpleCharge, Factura factura, List<Item> listaItems, string client_id, string client_secret)
        {
            FourGeeksPayments payments = new FourGeeksPayments(client_id,client_secret);

            using (var db = new BD_ProyectoValkyriaContext())
            {
                if (null != factura)
                {
                    db.Facturas.Add(factura);
                    int rowsAffected = db.SaveChanges();

                    if (rowsAffected > 0)
                    {
                        if (null != listaItems)
                        {
                            foreach(Item item in listaItems)
                            {
                                FacturaDetalle facDetalle = new FacturaDetalle();

                                facDetalle.IdFactura = factura.IdFactura;
                                facDetalle.Precio = item.Precio;
                                facDetalle.Cantidad = item.Cantidad;

                                switch (item.TipoItem)
                                {
                                    case "Product":

                                        facDetalle.IdProducto = item.IdItem;

                                        var resultado = (from p in db.Productos
                                                         where p.IdProducto == facDetalle.IdProducto
                                                         select p).FirstOrDefault();

                                        if (null != resultado)
                                        {
                                            if (null == resultado.CantVendidas)
                                            {
                                                resultado.CantVendidas = 1;
                                            }
                                            else 
                                            {
                                                resultado.CantVendidas++;
                                            }

                                            db.SaveChanges();
                                        }
                                        break;

                                    case "Workshop":

                                        facDetalle.IdWorkshop = item.IdItem;

                                        var resultadoW = (from p in db.Workshops
                                                         where p.IdWorkshop == facDetalle.IdWorkshop
                                                         select p).FirstOrDefault();

                                        if (null != resultadoW)
                                        {
                                            if (null == resultadoW.CuposVendidos)
                                            {
                                                resultadoW.CuposVendidos = 1;
                                            }
                                            else
                                            {
                                                resultadoW.CuposVendidos++;
                                            }

                                            
                                            db.SaveChanges();
                                        }

                                        break;

                                    case "Actividad":

                                        facDetalle.IdActividad = item.IdItem;

                                        var resultadoA = (from p in db.Actividades
                                                          where p.IdActividades == facDetalle.IdActividad
                                                          select p).FirstOrDefault();

                                        if (null != resultadoA)
                                        {
                                            if (null == resultadoA.CuposVendidos)
                                            {
                                                resultadoA.CuposVendidos = 1;
                                            }
                                            else
                                            {
                                                resultadoA.CuposVendidos++;
                                            }
                                            db.SaveChanges();
                                        }


                                        break;

                                    case "Programa":

                                        facDetalle.IdPrograma = item.IdItem;

                                        var resultadoP = (from p in db.Programas
                                                          where p.IdPrograma == facDetalle.IdPrograma
                                                          select p).FirstOrDefault();

                                        if (null != resultadoP)
                                        {
                                            if (null == resultadoP.CantVendidas)
                                            {
                                                resultadoP.CantVendidas = 1;
                                            }
                                            else
                                            {
                                                resultadoP.CantVendidas++;
                                            }
                                            
                                            db.SaveChanges();
                                        }

                                        break;
                                }

                                db.FacturaDetalles.Add(facDetalle);
                                db.SaveChanges();
                            }

                        }


                    }
                }
                


            }
                return payments.CreateSimpleCharge(simpleCharge); //if true everything worked fine
        }
    }
}
