﻿using FrontEndVakyria.Modelos_BD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEndVakyria.Models
{
    public class ActividadModelo
    {
        public void IngresarActividad(Actividade actividad)
        {
            using (var db = new BD_ProyectoValkyriaContext())
            {
                db.Actividades.Add(actividad);
                db.SaveChanges();

            }
        }

        public List<Actividade> CargarActividades()
        {
            List<Actividade> lista = new List<Actividade>();
            using (var db = new BD_ProyectoValkyriaContext())
            {
                var resultado = (from ac in db.Actividades
                                 select new
                                 {
                                     id = ac.IdActividades,
                                     nombre = ac.NombreActividades,
                                     tipo = ac.TipoActividad,
                                     descripcion = ac.Descripcion,
                                     minInventario = ac.MinInventario,
                                     maxInventario = ac.MaxInventario,
                                     Precio = ac.Precio,
                                     Fecha = ac.Fecha
                                 }
                                 ).ToList().GroupBy(x => x.id).Select(g => g.First());

                foreach (var item in resultado)
                {
                    lista.Add(new Actividade
                    {
                        IdActividades = item.id,
                        NombreActividades = item.nombre,
                        TipoActividad = item.tipo,
                        Descripcion = item.descripcion,
                        MinInventario = item.minInventario,
                        MaxInventario = item.maxInventario,
                        Precio = item.Precio,
                        Fecha = item.Fecha

                    });
                }
            }

            return lista;
        }


        public Actividade CargarActividadPorId(int idActividad)
        {
            using (var db = new BD_ProyectoValkyriaContext())
            {
                if (idActividad <= 0)
                    return null;

                var resultado = (from ac in db.Actividades
                                 where ac.IdActividades == idActividad
                                 select new
                                 {
                                     id = ac.IdActividades,
                                     nombre = ac.NombreActividades,
                                     descripcion = ac.Descripcion,
                                     minInventario = ac.MinInventario,
                                     maxInventario = ac.MaxInventario,
                                     tipo = ac.TipoActividad,
                                     Precio = ac.Precio,
                                     Fecha = ac.Fecha
                                 }).FirstOrDefault();

                if (null != resultado)
                {
                    Actividade work = new Actividade();
                    work.IdActividades = resultado.id;
                    work.NombreActividades = resultado.nombre;
                    work.Descripcion = resultado.descripcion;
                    work.MinInventario = resultado.minInventario;
                    work.MaxInventario = resultado.maxInventario;
                    work.TipoActividad = resultado.tipo;
                    work.Precio = resultado.Precio;
                    work.Fecha = resultado.Fecha;
                 

                    return work;
                }

                return null;
            }

        }

        public int ModificarActividad(Actividade actividad)
        {
            using (var db = new BD_ProyectoValkyriaContext())
            {

                if (null == actividad || actividad.IdActividades <= 0)
                    return 0;

                var resultado = (from ac in db.Actividades
                                 where ac.IdActividades == actividad.IdActividades
                                 select ac).FirstOrDefault();

                if (null != resultado)
                {
                    resultado.NombreActividades = actividad.NombreActividades;
                    resultado.Precio = actividad.Precio;
                    resultado.Fecha = actividad.Fecha;
                    resultado.Descripcion = actividad.Descripcion;
                    resultado.TipoActividad = actividad.TipoActividad;
                    resultado.MinInventario = actividad.MinInventario;
                    resultado.MaxInventario = actividad.MaxInventario;
                    return db.SaveChanges();
                }

                return 0;

            }
        }

        public int EliminarActividad(int idAct)
        {
            using (var db = new BD_ProyectoValkyriaContext())
            {
                if (idAct <= 0)
                    return 0;

                var resultado = (from ac in db.Actividades
                                 where ac.IdActividades == idAct
                                 select ac).FirstOrDefault();

                if (null != resultado)
                {
                    db.Actividades.Remove(resultado);
                    return db.SaveChanges();
                }

                return 0;
            }

        }

        public decimal totalActividades()
        {

            using (var db = new BD_ProyectoValkyriaContext())
            {
                var sums = from x in db.FacturaDetalles
                           where x.IdActividad != null
                           select new
                           {
                               Precio = db.FacturaDetalles.Where(n => n.IdActividad != null).Sum(n => n.Precio)
                           };
                var result = sums.FirstOrDefault();
                return (decimal)result.Precio;

            }
        }

        public decimal totalTalleres()
        {

            using (var db = new BD_ProyectoValkyriaContext())
            {
                var sums = from x in db.FacturaDetalles
                           join y in db.Actividades on x.IdActividad equals y.IdActividades
                           where y.TipoActividad == 1
                           group x by new { y.TipoActividad, x.Precio } into g
                           select new
                           {
                               Precio = g.Sum(x => x.Precio)
                           };
                var result = sums.FirstOrDefault();
                return (decimal)result.Precio;

            }
        }

        public decimal totalEventos()
        {

            using (var db = new BD_ProyectoValkyriaContext())
            {
                var sums = from x in db.FacturaDetalles
                           join y in db.Actividades on x.IdActividad equals y.IdActividades
                           where y.TipoActividad == 2
                           group x by new { y.TipoActividad, x.Precio } into g
                           select new
                           {
                               Precio = g.Sum(x => x.Precio)
                           };
                var result = sums.FirstOrDefault();
                return (decimal)result.Precio;

            }
        }

        public int cuposDisponibles()
        {

            using (var db = new BD_ProyectoValkyriaContext())
            {
                var sums = from x in db.Actividades
                           select new
                           {
                               Cupos = db.Actividades.Sum(n => n.MaxInventario),
                               Vendidos = db.Actividades.Sum(n => n.CuposVendidos)
                           };
                var result = sums.FirstOrDefault();
                var total = result.Cupos - result.Vendidos;
                if (result != null)
                {
                    return (int)total;
                }
                else
                {
                    return 0;
                }

            }
        }

        public int cuposVendidos()
        {

            using (var db = new BD_ProyectoValkyriaContext())
            {
                var sums = from x in db.Actividades
                           select new
                           {
                               Vendidos = db.Actividades.Sum(n => n.CuposVendidos)
                           };
                var result = sums.FirstOrDefault();
                return (int)result.Vendidos;

            }
        }

        public int cuposTalleres()
        {

            using (var db = new BD_ProyectoValkyriaContext())
            {
                var sums = from x in db.Actividades
                           where x.TipoActividad == 1
                           select new
                           {
                               Total = db.Actividades.Where(n => n.TipoActividad == 1).Sum(n => n.CuposVendidos)
                           };
                var result = sums.FirstOrDefault();
                return (int)result.Total;

            }
        }

        public int cuposEventos()
        {

            using (var db = new BD_ProyectoValkyriaContext())
            {
                var sums = from x in db.Actividades
                           where x.TipoActividad == 2
                           select new
                           {
                               Total = db.Actividades.Where(n => n.TipoActividad == 2).Sum(n => n.CuposVendidos)
                           };
                var result = sums.FirstOrDefault();
                
                if (result != null)
                {
                    return (int)result.Total;
                }
                else
                {
                    return 0;
                }
            }
        }


    }




}
