﻿using FrontEndVakyria.Entities;
using FrontEndVakyria.Modelos_BD;
using FrontEndVakyria.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEndVakyria.Controllers
{
    public class AdminProgramaController : Controller
    {
        LogModelo logMod = new LogModelo();
        Log log = new Log();
        static string mensajeError = "";
        static bool itFailed = false;

        public void RegistrarLog(String descripcion, String modulo, String accion)
        {
            try
            {
                log.Fecha = DateTime.Now;
                log.Descripcion = descripcion;
                log.Modulo = modulo;
                log.Accion = accion;
                if (null == ViewBag.User)
                {
                    log.Usuario = "Invitado";
                }
                else
                {
                    log.Usuario = ViewBag.User;
                }
                logMod.IngresarLog(log);
            }
            catch (Exception e)
            {
               
            }

        }


        [HttpPost]
        public IActionResult InsertarPrograma(Programa Programa)
        {
            try
            {
                ////Programa = null;
                ProgramaModelo prodMod = new ProgramaModelo();
                prodMod.IngresarPrograma(Programa);

                RegistrarLog("Se ingresó el Programa: "+Programa.NombrePrograma, "AdminPrograma", "InsertarPrograma");

                return RedirectToAction("Consultar", "AdminPrograma");
            }
            catch (Exception e)
            {
                RegistrarLog(e.ToString().Substring(0,299), "AdminPrograma", "InsertarPrograma");
                return RedirectToAction("Consultar", "AdminPrograma");
            }

        }


        [HttpGet]
        public IActionResult Insertar()
        {
            ViewBag.Imagenes = CargarImagenes();
            return View();
        }

        [HttpGet]
        public IActionResult Consultar()
        {
            try
            {
                if (mensajeError != null && mensajeError != "" && itFailed == false)
                {
                    itFailed = true;
                    ViewBag.Mensaje = mensajeError;
                }
                else if (itFailed == true && mensajeError != "")
                {
                    itFailed = false;
                    mensajeError = "";
                }

                ProgramaModelo prodMod = new ProgramaModelo();
                return View(prodMod.CargarProgramas());

            }
            catch (Exception e)
            {

                RegistrarLog(e.ToString().Substring(0, 299), "AdminPrograma", "Consultar");

                return View();
            }

        }


        [HttpPost]
        public String ConsultarPorId(int IdPrograma)
        {
            try
            {
                ProgramaModelo prodMod = new ProgramaModelo();
                Programa Programa = new Programa();
                Programa = prodMod.CargarProgramaPorId(IdPrograma);

                if (null != Programa)
                {
                    return JsonConvert.SerializeObject(Programa);
                }
                else
                {
                    return JsonConvert.SerializeObject(null);
                }

            }
            catch (Exception e)
            {
                RegistrarLog(e.InnerException.ToString(), "AdminPrograma", "ConsultarPorId");

                return JsonConvert.SerializeObject(null);
            }

        }

        [HttpGet]
        public IActionResult Actualizar()
        {
            try
            {
                ProgramaModelo prodMod = new ProgramaModelo();
                var listaProgramas = prodMod.CargarProgramas();

                List<SelectListItem> ListaCombo = new List<SelectListItem>();
                ListaCombo.Add(new SelectListItem
                {
                    Value = "0",
                    Text = "Seleccione un programa"
                });
                foreach (var item in listaProgramas)
                {
                    ListaCombo.Add(new SelectListItem
                    {
                        Value = item.IdPrograma.ToString(),
                        Text = item.NombrePrograma
                    });
                }

                ViewBag.Imagenes = CargarImagenes();
                ViewBag.ListaProgramas = ListaCombo.ToList();;
                return View();
            }
            catch (Exception e)
            {
                RegistrarLog(e.ToString().Substring(0, 299), "AdminPrograma", "Actualizar");

                return View();
            }

        }

        public List<SelectListItem> CargarImagenes()
        {
            try
            {
                ImagenModelo imMod = new ImagenModelo();
                var listaImagenes = imMod.CargarImagenes();

                List<SelectListItem> ListaCombo = new List<SelectListItem>();
                ListaCombo.Add(new SelectListItem
                {
                    Value = "0",
                    Text = "Seleccione una imagen"
                });
                foreach (var item in listaImagenes)
                {
                    ListaCombo.Add(new SelectListItem
                    {
                        Value = item.IdImagen.ToString(),
                        Text = item.NombreImagen
                    });
                }

                ViewBag.ListaProgramas = ListaCombo.ToList();

                return ListaCombo;
            }
            catch (Exception e)
            {

                RegistrarLog(e.ToString().Substring(0, 299), "AdminPrograma", "CargarImagenes");

                return new List<SelectListItem>(); ;
            }

        }



        [HttpPost]
        public IActionResult ActualizarPrograma(Programa Programa)
        {
            try
            {
                ProgramaModelo prodMod = new ProgramaModelo();
                int rowsAfectados = prodMod.ModificarPrograma(Programa);

                if (rowsAfectados > 0)
                {
                    RegistrarLog("Se actualizó el Programa: " + Programa.IdPrograma.ToString(), "AdminPrograma", "ActualizarPrograma");
                    return RedirectToAction("Consultar", "AdminPrograma"); //Actualizo un registro vamos a Consulta
                }
                else
                {
                    return RedirectToAction("Actualizar", "AdminPrograma"); //Si hubo problema, volver al view
                }
            }
            catch (Exception e)
            {

                RegistrarLog(e.InnerException.ToString(), "AdminPrograma", "ActualizarPrograma");

                return RedirectToAction("Consultar", "AdminPrograma");
            }

        }

        
        [HttpGet]
        public IActionResult Imagen()
        {
            try
            {
                ImagenModelo imMod = new ImagenModelo();
                var listaImagenes = imMod.CargarImagenes();

                List<TipoImagen> listaReal = new List<TipoImagen>();

                foreach (var item in listaImagenes)
                {
                    listaReal.Add(new TipoImagen
                    {
                        IdImagen = item.IdImagen,
                        NombreImagen = item.NombreImagen,
                        DatosImagen = item.DatosImagen,
                        imgDataURL = string.Format("data:image/png;base64,{0}", Convert.ToBase64String(item.DatosImagen))


                    });
                }

                return View(listaReal);
            }
            catch (Exception e)
            {
                RegistrarLog(e.InnerException.ToString(), "AdminPrograma", "Imagen");

                return RedirectToAction("Consultar", "AdminPrograma");
            }


        }



        [HttpPost]
        public String EliminarImagen(int IdImagen)
        {
            try
            {
                ImagenModelo imgMod = new ImagenModelo();
                int rowsAfectados = imgMod.EliminarImagen(IdImagen);

                if (rowsAfectados > 0)
                {
                    RegistrarLog("Se eliminó la imagen: " + IdImagen, "AdminPrograma", "EliminarImagen");
                    return JsonConvert.SerializeObject(rowsAfectados);
                }
                else
                {
                    return JsonConvert.SerializeObject(null);
                }
            }
            catch (Exception e)
            {
                RegistrarLog(e.ToString().Substring(0, 299), "AdminPrograma", "EliminarImagen");

                return JsonConvert.SerializeObject(null);
            }

        }

        [HttpPost]
        public String EliminarPrograma(int IdProd)
        {
            try
            {
                ProgramaModelo prodMod = new ProgramaModelo();
                int rowsAfectados = prodMod.EliminarPrograma(IdProd);

                if (rowsAfectados > 0) { 
                    RegistrarLog("Se eliminó el Programa: " + IdProd, "AdminPrograma", "EliminarPrograma");
                    return JsonConvert.SerializeObject(rowsAfectados);
                }
                else
                {
                    return JsonConvert.SerializeObject(null);
                }
            }
            catch (Exception e)
            {

                if (e.InnerException.Message.Contains("a foreign key constraint fails"))
                {
                    RegistrarLog(e.ToString().Substring(0, 299), "AdminPrograma", "EliminarPrograma");
                    mensajeError = "El programa no se pudo eliminar porque ya ha sido adquirido por nuestros clientes";
                    itFailed = false;
                    return JsonConvert.SerializeObject(null);
                }

                RegistrarLog(e.ToString().Substring(0, 299), "AdminPrograma", "EliminarPrograma");
                return JsonConvert.SerializeObject(null);
            }

        }
        [HttpGet]
        public IActionResult ReportesProgramas()
        {

            try
            {
                ProgramaModelo progMod = new ProgramaModelo();



                var totalVentas = progMod.totalVentas();
                HttpContext.Session.SetString("totalVentas", totalVentas.ToString());

                var totalVentasSemana = progMod.totalVentasSemana();
                HttpContext.Session.SetString("totalVentasSemana", totalVentasSemana.ToString());


                var cantProgramas = progMod.cantProgramas();
                HttpContext.Session.SetString("cantProgramas", cantProgramas.ToString());

                return View(progMod.CargarProgramas());

            }
            catch (Exception e)
            {

                RegistrarLog(e.ToString().Substring(0, 299), "AdminPrograma", "ReportesProgramas");
                return View();
            }

        }
    }
}
