﻿using EmailService;
using PasswordGenerator;
using FrontEndVakyria.Modelos_BD;
using FrontEndVakyria.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace FrontEndVakyria.Controllers
{
    public class LoginController : Controller
    {

        LogModelo logMod = new LogModelo();
        Log log = new Log();
        private readonly IEmailSender _emailSender;

        public LoginController(IEmailSender emailSender)
        {
            _emailSender = emailSender;
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Usuario user)
        {
            LoginModelo login = new LoginModelo();
            var resultado = login.ValidarLogin(user);

            //HttpContext.Session.SetString("id", null);
            //HttpContext.Session.SetString("nombre", null);
            //HttpContext.Session.SetString("apellido1", null);
            //HttpContext.Session.SetString("apellido2", null);
            //HttpContext.Session.SetString("cedula", null);
            //HttpContext.Session.SetString("correo", null);
            //HttpContext.Session.SetString("telefono", null);
            //HttpContext.Session.SetString("contrasena", null);
            //HttpContext.Session.SetString("rol", null);
            //HttpContext.Session.SetString("ultimaconn", null);
            //ViewBag.ErrorLogin = string.Empty;

            if (resultado != null)
            {
                HttpContext.Session.SetString("id", resultado.IdUsuario.ToString());
                HttpContext.Session.SetString("nombre", resultado.NombreUsuario.ToString());
                HttpContext.Session.SetString("apellido1", resultado.PrimerApellidoUsuario.ToString());
                HttpContext.Session.SetString("apellido2", resultado.SegundoApellidoUsuario.ToString());
                HttpContext.Session.SetString("cedula", resultado.CedulaUsuario.ToString());
                HttpContext.Session.SetString("correo", resultado.CorreoElectronico.ToString());
                HttpContext.Session.SetString("telefono", resultado.TelefonoUsuario.ToString());
                HttpContext.Session.SetString("contrasena", resultado.ContrasenaUsuario.ToString());
                HttpContext.Session.SetString("rol", resultado.RolUsuario.ToString());
                HttpContext.Session.SetString("ultimaconn", resultado.UltimaConexion.ToString());

                ViewBag.User = resultado.CedulaUsuario.ToString();

                if (resultado.RolUsuario == 333)
                {
                    return RedirectToAction("Account", "User");
                }
                else
                {
                    return RedirectToAction("AdminInicio", "Admin");
                }
                
            }
            else
            {
                ViewBag.Message = string.Format("Por favor verifique sus credenciales");
                return View();
            }
        }

        public IActionResult LoginSuccess()
        {
            return RedirectToAction("Account", "User");
            //return View();
        }

        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            ViewBag.User = null;
            return RedirectToAction("Home", "Home");
            //return View();
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]

        public IActionResult RegisterUser(Usuario usuario)
        {
            try
            {
                LoginModelo model = new LoginModelo();

                
                  
                if (usuario.CorreoElectronico != null)
                {

                    model.RegistrarUsuario(usuario);

                    RegistrarLog("Usuario Registrado: " + usuario.CorreoElectronico, "Login", "RegisterUser");
                    return RedirectToAction("Login", "Login");
                }
                else
                {
                    RegistrarLog("No se pudo registrar el correo es nulo", "Login", "RegisterUser");
                    return RedirectToAction("Register", "Login");
                }


            }
            catch (Exception e)
            {
                RegistrarLog(e.ToString().Substring(0, 299), "Login", "RegisterUser");
                return RedirectToAction("Register", "Login");
            }
        }

        

        [HttpPost]
        public JsonResult ValidatingEmail(string CorreoElectronico)
        {
            try
            {

                LoginModelo model = new LoginModelo();

                var resultado = model.ValidarRegistro(CorreoElectronico);


                if (resultado == null)
                {
                    RegistrarLog("Correo Validado", "Login", "Validating Email");
                    return Json(null);
                }
                else
                {
                    RegistrarLog("No se pudo validar correo", "Login", "Validating Email");
                    return Json((new { success = false, responseText = "Hay un problema con el correo" }));
                }

            }
            catch (Exception e)
            {
                RegistrarLog(e.ToString().Substring(0, 299), "Login", "ValidatingEmail");
                return Json(null);
            }
        }

        [HttpGet]
        public IActionResult RetrievePassword(string? status)
        {
            if (status == "NotFound")
            {
                return View(true);
            }
            else
            {
                return View(false);
            }
        }

        [HttpPost]
        public IActionResult GetNewCredentials(string cedula)
        {
            try
            {
                LoginModelo mod = new LoginModelo();
                Usuario usuario = mod.ConseguirUsuarioConCedula(cedula); //cargamos usuario con la cedla

                if (null == usuario)
                {
                    return RedirectToAction("RetrievePassword", "Login", new { status="NotFound" });
                }

                RandomPasswordGenerator passwordGenerator = new RandomPasswordGenerator();
                string nuevaContrasena = passwordGenerator.GeneratePassword(true,true,true,true,8);

                usuario.ContrasenaUsuario = nuevaContrasena;
                
                mod.ActualizarContrasenaUsuario(usuario);

                if (null != usuario)
                {
                    // var files = Request.Form.Files.Any() ? Request.Form.Files : new FormFileCollection(); for a post method
                    var message = new Message(new string[] { usuario.CorreoElectronico }, "Proyecto Valkyria: solicitud de cambio de contraseña", "Su nueva contraseña es: " + nuevaContrasena, null);
                    _emailSender.SendEmail(message);

                }

                return View();
            }
            catch (Exception e)
            {
                RegistrarLog(e.ToString().Substring(0, 299), "Login", "GetNewCredentials");
                return RedirectToAction("Login", "Login");
            }
            
        }



        public void RegistrarLog(String descripcion, String modulo, String accion)
        {
            try
            {
                log.Fecha = DateTime.Now;
                log.Descripcion = descripcion;
                log.Modulo = modulo;
                log.Accion = accion;
                if (null == ViewBag.User)
                {
                    log.Usuario = "Invitado";
                }
                else
                {
                    log.Usuario = ViewBag.User;
                }
                logMod.IngresarLog(log);
            }
            catch (Exception e)
            {

            }

        }

    }
}
