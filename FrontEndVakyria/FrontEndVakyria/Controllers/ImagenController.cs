﻿using FrontEndVakyria.ActionFilters;
using FrontEndVakyria.Modelos_BD;
using FrontEndVakyria.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEndVakyria.Controllers
{
    [VerificarSesion]
    public class ImagenController : Controller
    {

        [HttpPost]
        public async Task<IActionResult> InsertarImagenes(List<IFormFile> files, String NombreArchivo)
        {
            ImagenModelo imgMod = new ImagenModelo();
            try
            {
                long size = files.Sum(f => f.Length);

                var filePaths = new List<string>();
                foreach (var formFile in files)
                {
                    if (formFile.Length > 0)
                    {
                        // full path to file in temp location
                        var filePath = Path.GetTempFileName(); //we are using Temp file name just for the example. Add your own file path.
                        filePaths.Add(filePath);

                        using (var memoryStream = new MemoryStream())
                        {
                            await formFile.CopyToAsync(memoryStream);

                            if (memoryStream.Length < 2097152)
                            {
                                var file = new Imagen()
                                {
                                    NombreImagen = NombreArchivo,
                                    DatosImagen = memoryStream.ToArray()
                                };

                                imgMod.IngresarImagen(file);

                            }
                            else
                            {
                                ModelState.AddModelError("File", "The file is too large.");
                            }
                        }
                    }
                }
                return RedirectToAction("Consultar", "AdminProducto");
            }
            catch (Exception e)
            {
                e.GetBaseException();
                String mensaje = e.Message;
                var innerMessage = e.InnerException;
                return RedirectToAction("Consultar", "AdminProducto");
            }
        }
        

        [HttpPost]
        public IActionResult InsertarImagen(Imagen imagen)
        {
            try
            {
                ImagenModelo imgMod = new ImagenModelo();
                imgMod.IngresarImagen(imagen);
                return RedirectToAction("Consultar", "AdminProducto");
            }
            catch (Exception e)
            {
                e.GetBaseException();
                String mensaje = e.Message;
                var innerMessage = e.InnerException;
                return RedirectToAction("Consultar", "AdminProducto");
            }

        }

    }
}
