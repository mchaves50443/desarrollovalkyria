﻿using FrontEndVakyria.ActionFilters;
using FrontEndVakyria.Modelos_BD;
using FrontEndVakyria.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEndVakyria.Controllers
{
    //[VerificarSesion]

    public class UserController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Account()
        {
            return View();
        }


        [HttpGet]
        public IActionResult ShoppingHistory()
        {
            string id = HttpContext.Session.GetString("id");
            ShopModel model = new ShopModel();
            return View(model.misCompras(id));
        }

      
        [HttpPost]
        public IActionResult UpdateUser(Usuario usuario)
        {
            try
            {
                string id = HttpContext.Session.GetString("id");
                usuario.IdUsuario = long.Parse(id);
                UsuarioModelo prodUsuario = new UsuarioModelo();
                int rowsAfectados = prodUsuario.ModificarUsuario(usuario);

                if (rowsAfectados > 0)
                {
                    HttpContext.Session.SetString("id", usuario.IdUsuario.ToString());
                    HttpContext.Session.SetString("nombre", usuario.NombreUsuario.ToString());
                    HttpContext.Session.SetString("apellido1", usuario.PrimerApellidoUsuario.ToString());
                    HttpContext.Session.SetString("apellido2", usuario.SegundoApellidoUsuario.ToString());
                    HttpContext.Session.SetString("cedula", usuario.CedulaUsuario.ToString());
                    HttpContext.Session.SetString("correo", usuario.CorreoElectronico.ToString());
                    HttpContext.Session.SetString("telefono", usuario.TelefonoUsuario.ToString());
                    HttpContext.Session.SetString("contrasena", usuario.ContrasenaUsuario.ToString());
                    HttpContext.Session.SetString("rol", usuario.RolUsuario.ToString());
                    HttpContext.Session.SetString("ultimaconn", usuario.UltimaConexion.ToString());
                    return RedirectToAction("Account", "User"); //Actualizo un registro vamos a Consulta
                    
                }
                else
                {
                    return RedirectToAction("Account", "User"); //Si hubo problema, volver al view
                }
            }
            catch (Exception)
            {
                //Agregar log
                return RedirectToAction("Account", "User");
            }

        }
    }
}
