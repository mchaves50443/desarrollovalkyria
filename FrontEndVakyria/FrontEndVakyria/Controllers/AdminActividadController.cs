﻿using FrontEndVakyria.Modelos_BD;
using FrontEndVakyria.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEndVakyria.Controllers
{
    public class AdminActividadController : Controller
    {
        LogModelo logMod = new LogModelo();
        Log log = new Log();
        static string mensajeError = "";
        static bool itFailed = false;


        public void RegistrarLog(String descripcion, String modulo, String accion)
        {
            try
            {
                log.Fecha = DateTime.Now;
                log.Descripcion = descripcion;
                log.Modulo = modulo;
                log.Accion = accion;
                if (null == ViewBag.User)
                {
                    log.Usuario = "Invitado";
                }
                else
                {
                    log.Usuario = ViewBag.User;
                }
                logMod.IngresarLog(log);
            }
            catch (Exception e)
            {

            }

        }

        public List<SelectListItem> ConseguirLista()
        {
            List<SelectListItem> lista = new List<SelectListItem>();

            lista.Add(new SelectListItem { Value = "0", Text = "--Porfavor Seleccionar--" });
            lista.Add(new SelectListItem { Value = "1", Text = "Taller" });
            lista.Add(new SelectListItem { Value = "2", Text = "Otro" });
           

            return lista;
        }

        [HttpGet]
        public IActionResult AgregarActividad()
        {
            
            ViewBag.TipoActividad = ConseguirLista();

            return View();
        }

        [HttpPost]
        public IActionResult InsertarActividad(Actividade actividad)
        {
            try
            {
                ActividadModelo acMod = new ActividadModelo();
                acMod.IngresarActividad(actividad);
                return RedirectToAction("ListaActividades", "AdminActividad");
            }
            catch (Exception e)
            {
                RegistrarLog(e.ToString().Substring(0, 299), "AdminActividad", "InsertarActividad");
                return RedirectToAction("ListaActividades", "AdminActividad");
            }

        }

        [HttpGet]
        public IActionResult ListaActividades()
        {

            try
            {
                if (mensajeError != null && mensajeError != "" && itFailed == false)
                {
                    itFailed = true;
                    ViewBag.Mensaje = mensajeError;
                }
                else if (itFailed == true && mensajeError != "")
                {
                    itFailed = false;
                    mensajeError = "";
                }

                ActividadModelo acMod = new ActividadModelo();
                ViewBag.TipoActividad = ConseguirLista();

                return View(acMod.CargarActividades());
            }
            catch (Exception e)
            {
                RegistrarLog(e.ToString().Substring(0, 299), "AdminActividad", "ListaActividades");
                return View();

            }
            
        }

        [HttpPost]
        public String ConsultarPorId(int IdActividad)
        {
            try
            {
                ActividadModelo prodMod = new ActividadModelo();
                Actividade actividad = new Actividade();
                actividad = prodMod.CargarActividadPorId(IdActividad);

                if (null != actividad)
                {
                    return JsonConvert.SerializeObject(actividad);
                }
                else
                {
                    return JsonConvert.SerializeObject(null);
                }

            }
            catch (Exception e)
            {
                RegistrarLog(e.ToString().Substring(0, 299), "AdminActividad", "ConsultarPorId");
                //Agregar Log de Errores
                return JsonConvert.SerializeObject(null);
            }

        }


        public IActionResult ModificarActividad()
        {
            ActividadModelo acMod = new ActividadModelo();
            var listaProductos = acMod.CargarActividades();

            List<SelectListItem> ListaCombo = new List<SelectListItem>();
            foreach (var item in listaProductos)
            {
                ListaCombo.Add(new SelectListItem
                {
                    Value = item.IdActividades.ToString(),
                    Text = item.NombreActividades
                });
            }

           
            ViewBag.ListaActividades = ListaCombo.ToList();
            ViewBag.TipoActividad = ConseguirLista();
            return View();
        }

        [HttpPost]
        public IActionResult ActualizarActividad(Actividade actividad)
        {
            try
            {
                ActividadModelo acMod = new ActividadModelo();
                int rowsAfectados = acMod.ModificarActividad(actividad);

                if (rowsAfectados > 0)
                {
                    return RedirectToAction("ListaActividades", "AdminActividad"); //Actualizo un registro vamos a Consulta
                }
                else
                {
                    return RedirectToAction("ModificarActividad", "AdminActividad"); //Si hubo problema, volver al view
                }
            }
            catch (Exception e)
            {
                RegistrarLog(e.ToString().Substring(0, 299), "AdminActividad", "ActualizarActividad");
                //Agregar log
                return RedirectToAction("ListaActividades", "AdminActividad");
            }

        }

        [HttpPost]
        public String EliminarActividad(int IdAct)
        {
            try
            {
                ActividadModelo actMod = new ActividadModelo();
                int rowsAfectados = actMod.EliminarActividad(IdAct);

                if (rowsAfectados > 0)
                {
                    return JsonConvert.SerializeObject(rowsAfectados);
                }
                else
                {
                    return JsonConvert.SerializeObject(null);
                }
            }
            catch (Exception e)
            {
                if (e.InnerException.Message.Contains("a foreign key constraint fails"))
                {
                    RegistrarLog(e.ToString().Substring(0, 299), "AdminActividad", "EliminarActividad");
                    mensajeError = "La actividad no se pudo eliminar porque ya ha sido adquirida por nuestros clientes";
                    itFailed = false;
                    return JsonConvert.SerializeObject(null);
                }
                RegistrarLog(e.ToString().Substring(0, 299), "AdminActividad", "EliminarActividad");
                //Agregar log de errores
                return JsonConvert.SerializeObject(null);
            }

        }

        public IActionResult ReportesActividades()
        {
            ActividadModelo actMod = new ActividadModelo();
            var suma = actMod.totalActividades();
            HttpContext.Session.SetString("totalActividades", suma.ToString());

            var sumaTalleres = actMod.totalTalleres();
            HttpContext.Session.SetString("totalTalleres", sumaTalleres.ToString());

            var sumaEventos = actMod.totalEventos();
            HttpContext.Session.SetString("totalEventos", sumaEventos.ToString());

            var cuposDisponibles = actMod.cuposDisponibles();
            HttpContext.Session.SetString("cuposDisponibles", cuposDisponibles.ToString());

            var cuposVendidos = actMod.cuposVendidos();
            HttpContext.Session.SetString("cuposVendidos", cuposVendidos.ToString());

            var cuposTalleres = actMod.cuposTalleres();
            HttpContext.Session.SetString("cuposTalleres", cuposTalleres.ToString());

            var cuposEventos = actMod.cuposEventos();
            HttpContext.Session.SetString("cuposEventos", cuposEventos.ToString());

            ViewBag.TipoActividad = ConseguirLista();
            return View(actMod.CargarActividades());
        }
    }
}
