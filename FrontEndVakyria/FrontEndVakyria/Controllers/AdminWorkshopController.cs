﻿using FrontEndVakyria.Modelos_BD;
using FrontEndVakyria.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEndVakyria.Controllers
{
    public class AdminWorkshopController : Controller
    {
        LogModelo logMod = new LogModelo();
        Log log = new Log();
        static string mensajeError = "";
        static bool itFailed = false;


        public void RegistrarLog(String descripcion, String modulo, String accion)
        {
            try
            {
                log.Fecha = DateTime.Now;
                log.Descripcion = descripcion;
                log.Modulo = modulo;
                log.Accion = accion;
                if (null == ViewBag.User)
                {
                    log.Usuario = "Invitado";
                }
                else
                {
                    log.Usuario = ViewBag.User;
                }
                logMod.IngresarLog(log);
            }
            catch (Exception e)
            {

            }

        }

        public List<SelectListItem> ConseguirLista()
        {
            List<SelectListItem> lista = new List<SelectListItem>();

            lista.Add(new SelectListItem { Value = "0", Text = "--Porfavor Seleccionar--" });
            lista.Add(new SelectListItem { Value = "1", Text = "Workshop" });

            return lista;
        }

        [HttpPost]
        public IActionResult InsertarWorkshop(Workshop workshop, string fechaInicio)
        {
            try
            {
                WorkshopModelo prodWork = new WorkshopModelo();
                workshop.Fecha = DateTime.Parse(fechaInicio);
                prodWork.IngresarWorkshop(workshop);
                return RedirectToAction("Consultar", "AdminWorkshop");
            }
            catch (Exception e)
            {
                RegistrarLog(e.ToString().Substring(0, 299), "AdminWorkshop", "InsertarWorkshop");
                return RedirectToAction("Consultar", "AdminWorkshop");
            }

        }

        [HttpGet]
        public IActionResult Insertar()
        {
            ViewBag.IdWorkshop = ConseguirLista();
            return View();
        }

        [HttpGet]
        public IActionResult Consultar()
        {
            try
            {
                if (mensajeError != null && mensajeError != "" && itFailed == false)
                {
                    itFailed = true;
                    ViewBag.Mensaje = mensajeError;
                }
                else if (itFailed == true && mensajeError != "")
                {
                    itFailed = false;
                    mensajeError = "";
                }

                ViewBag.IdWorkshop = ConseguirLista();
                WorkshopModelo prodWork = new WorkshopModelo();
                return View(prodWork.CargarWorkshop());

            }
            catch (Exception e)
            {
                RegistrarLog(e.ToString().Substring(0, 299), "AdminWorkshop", "ConsultarWorkshop");
                //Agregar Log de Errores
                return View();
            }

        }


        [HttpPost]
        public String ConsultarPorId(int IdWorkshop)
        {
            try
            {
                WorkshopModelo prodWork = new WorkshopModelo();
                Workshop workshop = new Workshop();
                workshop = prodWork.CargarWorkshopPorId(IdWorkshop);

                if (null != workshop)
                {
                    return JsonConvert.SerializeObject(workshop);
                }
                else
                {
                    return JsonConvert.SerializeObject(null);
                }

            }
            catch (Exception e)
            {
                //Agregar Log de Errores
                RegistrarLog(e.ToString().Substring(0, 299), "AdminWorkshop", "ConsultarPorId");
                return JsonConvert.SerializeObject(null);
            }

        }

        [HttpGet]
        public IActionResult Actualizar()
        {
            try
            {
                WorkshopModelo prodWork = new WorkshopModelo();
                var listaWorkshop = prodWork.CargarWorkshop();

                List<SelectListItem> ListaCombo = new List<SelectListItem>();
                foreach (var item in listaWorkshop)
                {
                    ListaCombo.Add(new SelectListItem
                    {
                        Value = item.IdWorkshop.ToString(),
                        Text = item.NombreWorkshop
                    });
                }

                ViewBag.ListaWorkshop = ListaCombo.ToList();

                return View();
            }
            catch (Exception e)
            {
                RegistrarLog(e.ToString().Substring(0, 299), "AdminWorkshop", "Actualizar");
                return View();
            }

        }

        [HttpPost]
        public IActionResult ActualizarWorkshop(Workshop workshop)
        {
            try
            {
                WorkshopModelo prodWork = new WorkshopModelo();
                int rowsAfectados = prodWork.ModificarWorkshop(workshop);

                if (rowsAfectados > 0)
                {
                    return RedirectToAction("Consultar", "AdminWorkshop"); //Actualizo un registro vamos a Consulta
                }
                else
                {
                    return RedirectToAction("Actualizar", "AdminWorkshop"); //Si hubo problema, volver al view
                }
            }
            catch (Exception e)
            {
                RegistrarLog(e.ToString().Substring(0, 299), "AdminWorkshop", "ActualizarWorkshop");
                //Agregar log
                return RedirectToAction("Consultar", "AdminWorkshop");
            }

        }

        [HttpGet]
        public IActionResult Eliminar()
        {
            return View();
        }

        [HttpPost]
        public String EliminarWorkshop(int IdWork)
        {
            try
            {
                WorkshopModelo prodWork = new WorkshopModelo();
                int rowsAfectados = prodWork.EliminarWorkshop(IdWork);

                if (rowsAfectados > 0)
                {
                    return JsonConvert.SerializeObject(rowsAfectados);
                }
                else
                {
                    return JsonConvert.SerializeObject(null);
                }
            }
            catch (Exception e)
            {
                if (e.InnerException.Message.Contains("a foreign key constraint fails"))
                {
                    RegistrarLog(e.ToString().Substring(0, 299), "AdminWorkshop", "EliminarWorkshop");
                    mensajeError = "El workshop no se pudo eliminar porque ya ha sido adquirido por nuestros clientes";
                    itFailed = false;
                    return JsonConvert.SerializeObject(null);
                }
                //Agregar log de errores
                RegistrarLog(e.ToString().Substring(0, 299), "AdminWorkshop", "EliminarWorkshop");
                return JsonConvert.SerializeObject(null);
            }

        }

        public List<SelectListItem> CargarImagenes()
        {

            ImagenModelo imMod = new ImagenModelo();
            var listaImagenes = imMod.CargarImagenes();

            List<SelectListItem> ListaCombo = new List<SelectListItem>();

            foreach (var item in listaImagenes)
            {
                ListaCombo.Add(new SelectListItem
                {
                    Value = item.IdImagen.ToString(),
                    Text = item.NombreImagen
                });
            }

            ViewBag.ListaProductos = ListaCombo.ToList();

            return ListaCombo;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Imagen()

        {
            return View();
        }

        public IActionResult ReportesWorkshop()

        {
            WorkshopModelo actMod = new WorkshopModelo();
            var suma = actMod.totalWorkshops();
            HttpContext.Session.SetString("totalWorkshops", suma.ToString());


            var sumaEventos = actMod.totalEventos();
            HttpContext.Session.SetString("totalEventos", sumaEventos.ToString());

            var cuposDisponibles = actMod.cuposDisponibles();
            HttpContext.Session.SetString("cuposDisponibles", cuposDisponibles.ToString());

            var cuposVendidos = actMod.cuposVendidos();
            HttpContext.Session.SetString("cuposVendidos", cuposVendidos.ToString());


            var cuposEventos = actMod.cuposEventos();
            HttpContext.Session.SetString("cuposEventos", cuposEventos.ToString());

            ViewBag.TipoActividad = ConseguirLista();
            return View(actMod.CargarWorkshop());
        }




    }
}