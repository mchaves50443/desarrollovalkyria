﻿using FrontEndVakyria.ActionFilters;
using FrontEndVakyria.Entities;
using FrontEndVakyria.Modelos_BD;
using FrontEndVakyria.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEndVakyria.Controllers
{
    [VerificarSesion]
    public class AdminController : Controller
    {

        Log log = new Log();
        LogModelo logMod = new LogModelo();
        public IActionResult Index()
        {
            return View();
        }


        public IActionResult AdminInicio()
        {
            ShopModel shopmod = new ShopModel();
            var suma = shopmod.Gananciastotales();
            HttpContext.Session.SetString("totalGanancias", suma.ToString());

            var ordenes = shopmod.OrdenesTotales();
            HttpContext.Session.SetString("totalOrdenes", ordenes.ToString());

            var productos = shopmod.ProductosTotales();
            HttpContext.Session.SetString("totalProductos", productos.ToString());

            LoginModelo logmod = new LoginModelo();
            var usuarios = logmod.totalUsuarios();
            HttpContext.Session.SetString("totalUsuarios", usuarios.ToString());
            return View();
        }




        public IActionResult Ordenes()
        {
            List<Orden> listaOrdenes = SessionHelper.GetObjectFromJson<List<Orden>>(HttpContext.Session, "listaOrdenes");

            if (null != listaOrdenes)
            {
                return View(listaOrdenes);
            }
            else
            {
                return View(null);
            }

        }


        [HttpPost]
        public IActionResult CargarOrdenes(string fechaInicio, string fechaFinal, string numeroOrden, string abierta, string cerrada)
        {
            OrdenModelo mod = new OrdenModelo();

            DateTime fechaIni = DateTime.Parse(fechaInicio);
            DateTime fechaFin = DateTime.Parse(fechaFinal);
            long id = 0;

            if ("" != numeroOrden && numeroOrden != null)
            {
                id = long.Parse(numeroOrden);
            }

            byte estado = 0; //por defecto se crean abiertos es decir con un cero
            if (abierta != "on" || cerrada == "on")
                estado = 1;

            List<Orden> listaOrdenes = new List<Orden>();

            if (id > 0 && id != null)
            {
                listaOrdenes = mod.CargarOrdenesPorId(id);
            }
            else
            {
                listaOrdenes = mod.CargarOrdenesConRango(fechaIni, fechaFin, estado);
            }

            SessionHelper.SetObjectAsJson(HttpContext.Session, "listaOrdenes", listaOrdenes);

            return RedirectToAction("Ordenes", "Admin");
        }

        [HttpPost]
        public IActionResult CerrarEstadoOrden(string idOrden)
        {
            OrdenModelo mod = new OrdenModelo();
            var rows = mod.ModificarOrden(Int32.Parse(idOrden), 1);

            return RedirectToAction("Ordenes", "Admin");
        }

        [HttpPost]
        public IActionResult AbrirEstadoOrden(string idOrden)
        {
            OrdenModelo mod = new OrdenModelo();
            var rows = mod.ModificarOrden(Int32.Parse(idOrden), 0);

            return RedirectToAction("Ordenes", "Admin");
        }

        [HttpGet]
        public IActionResult CargarConsultas()
        {
            ConsultaModelo cmod = new ConsultaModelo();

            return View(cmod.CargarConsultas());
        }
        [HttpPost]
        public string EliminarConsulta(int idCon)
        {
            try
            {
                ConsultaModelo conMod = new ConsultaModelo();
                int rowsAfectados = conMod.EliminarConsulta(idCon);

                if (rowsAfectados > 0)
                {
                    RegistrarLog("Se eliminó una consulta: " + idCon, "Admin", "EliminarConsulta");
                    return JsonConvert.SerializeObject(rowsAfectados);
                }
                else
                {
                    return JsonConvert.SerializeObject(null);
                }
            }
            catch (Exception e)
            {

                RegistrarLog(e.ToString().Substring(0, 299), "Admin", "EliminarConsultas");
                return JsonConvert.SerializeObject(null);
            }
        }

        public void RegistrarLog(String descripcion, String modulo, String accion)
        {
            try
            {
                log.Fecha = DateTime.Now;
                log.Descripcion = descripcion;
                log.Modulo = modulo;
                log.Accion = accion;
                if (null == ViewBag.User)
                {
                    log.Usuario = "Invitado";
                }
                else
                {
                    log.Usuario = ViewBag.User;
                }
                logMod.IngresarLog(log);
            }
            catch (Exception e)
            {

            }

        }

    }


}


