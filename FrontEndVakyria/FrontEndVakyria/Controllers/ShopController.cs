﻿using FrontEndVakyria.Entities;
using FrontEndVakyria.Modelos_BD;
using FrontEndVakyria.Models;
using gpayments;
using gpayments.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEndVakyria.Controllers
{
    public class ShopController : Controller
    {
        private readonly IConfiguration _config;
        public ShopController(IConfiguration config)
        {
            _config = config;
        }

        LogModelo logMod = new LogModelo();
        Log log = new Log();
        //List<Item>( ListItems = new List<Item>();
        public void RegistrarLog(String descripcion, String modulo, String accion)
        {
            try
            {
                log.Fecha = DateTime.Now;
                log.Descripcion = descripcion;
                log.Modulo = modulo;
                log.Accion = accion;
                if (null == ViewBag.User)
                {
                    log.Usuario = "Invitado";
                }
                else
                {
                    log.Usuario = ViewBag.User;
                }
                logMod.IngresarLog(log);
            }
            catch (Exception e)
            {

            }

        }
        public IActionResult Shop()
        {
            try
            {
                ProductoModelo prodMod = new ProductoModelo();

                List<ProductoImagen> listaReal = new List<ProductoImagen>();

                listaReal = prodMod.CargarProductosConImagen();

                return View(listaReal);

            }
            catch (Exception)
            {
                //Catch error logs
                return View();
            }

        }

        [HttpPost]
        public String CargarProductosConImagen()
        {
            try
            {
                ProductoModelo prodMod = new ProductoModelo();

                List<ProductoImagen> listaReal = new List<ProductoImagen>();

                listaReal = prodMod.CargarProductosConImagen();

                return JsonConvert.SerializeObject(listaReal);

            }
            catch (Exception)
            {
                //Catch error logs
                return JsonConvert.SerializeObject(null);
            }

        }


        [HttpPost]
        public String CargarProgramasConImagen()
        {
            try
            {
                ProgramaModelo proMod = new ProgramaModelo();

                List<ProgramaImagen> listaReal = new List<ProgramaImagen>();

                listaReal = proMod.CargarProgramasConImagen();

                return JsonConvert.SerializeObject(listaReal);

            }
            catch (Exception)
            {
                //Catch error logs
                return JsonConvert.SerializeObject(null);
            }

        }


        [HttpPost]
        public String RegistrarVenta(string provincia, string canton,
            string distrito, string direccion, string nombre, string apellidos, string correo,
            string telefono, string total, string codigo, string tipoItem, string cantidadItem, string precioItem, string zip,
            string credit_card_number, string credit_card_security_code_number, string exp_month, string exp_year)
        {
            try
            {

                string client_id = _config.GetValue<string>(
      "FourGeeks:client_id");

                string client_secret = _config.GetValue<string>(
                    "FourGeeks:client_secret");


                ShopModel mod = new ShopModel();
                Factura factura = new Factura();
                SimpleCharge simpleCharge = new SimpleCharge();
                List<Item> listaItems = new List<Item>();
                double amount = Double.Parse(total);

                string[] listaCodigos = codigo.Split("|", 100, StringSplitOptions.RemoveEmptyEntries);
                string[] listaTipos = tipoItem.Split("|", 100, StringSplitOptions.RemoveEmptyEntries);
                string[] listaCantidad = cantidadItem.Split("|", 100, StringSplitOptions.RemoveEmptyEntries);
                string[] listaPrecio = precioItem.Split("|", 100, StringSplitOptions.RemoveEmptyEntries);

                if (null != HttpContext.Session.GetString("id"))
                {
                    factura.IdUsuario = long.Parse(HttpContext.Session.GetString("id"));
                }

                if (listaCodigos.Length == listaTipos.Length)
                {
                    for (int i = 0; i < listaCodigos.Length; i++)
                    {
                        Item item = new Item();
                        item.IdItem = Int32.Parse(listaCodigos[i]);
                        item.TipoItem = listaTipos[i];
                        item.Cantidad = Int32.Parse(listaCantidad[i]);
                        item.Precio = Decimal.Parse(listaPrecio[i]);

                        listaItems.Add(item);

                    }
                }

                factura.NombreUsuario = nombre;
                factura.ApellidosUsuario = apellidos;
                factura.Provincia = provincia;
                factura.Canton = canton;
                factura.Distrito = distrito;
                factura.Direccion = distrito;
                factura.CodigoPostal = long.Parse(zip);
                factura.Telefono = telefono;
                factura.Correo = correo;
                factura.Total = Decimal.Parse(total);
                factura.Fecha = DateTime.Now;


                simpleCharge.Amount = amount;
                simpleCharge.CreditCardNumber = credit_card_number;
                simpleCharge.Currency = "crc";
                simpleCharge.CVC = credit_card_security_code_number;
                simpleCharge.Description = "Pago en tienda en línea Valkyria";
                simpleCharge.EntityDescription = "Tienda Valkyria";
                simpleCharge.ExpirationMonth = Int32.Parse(exp_month);
                simpleCharge.ExpirationYear = Int32.Parse(exp_year);

                bool result = mod.RegistrarPago(simpleCharge, factura, listaItems, client_id, client_secret);

                if (result == false)
                {
                    return JsonConvert.SerializeObject("failure");
                }
                else
                {
                    return JsonConvert.SerializeObject("success");
                }
            }
            catch (Exception e)
            {

                return JsonConvert.SerializeObject("error"); ;
            }
        }

        public String CargarWorkshopsConImagen()
        {
            try
            {
                WorkshopModelo workMod = new WorkshopModelo();

                List<WorkshopImagen> listaReal = new List<WorkshopImagen>();

                listaReal = workMod.CargarWorkshopsConImagen();
                return JsonConvert.SerializeObject(listaReal);
            }
            catch (Exception)
            {
                return JsonConvert.SerializeObject(null);
            }

        }


        public String CargarActividades()
        {
            try
            {
                ActividadModelo actMod = new ActividadModelo();

                List<Actividade> listaReal = new List<Actividade>();

                listaReal = actMod.CargarActividades();

                return JsonConvert.SerializeObject(listaReal);

            }
            catch (Exception)
            {
                //Catch error logs
                return JsonConvert.SerializeObject(null);
            }

        }


        public IActionResult Actividades()
        {
            try
            {
                ActividadModelo actMod = new ActividadModelo();

                List<Actividade> listaReal = new List<Actividade>();

                listaReal = actMod.CargarActividades();

                return View(listaReal);

            }
            catch (Exception)
            {
                //Catch error logs
                return View();
            }

        }

        public IActionResult Workshops()
        {
            try
            {
                WorkshopModelo workMod = new WorkshopModelo();

                List<WorkshopImagen> listaReal = new List<WorkshopImagen>();

                listaReal = workMod.CargarWorkshopsConImagen();

                return View(listaReal);

            }
            catch (Exception)
            {
                //Catch error logs
                return View();
            }

        }

        public IActionResult Programas()
        {
            try
            {
                ProgramaModelo prodMod = new ProgramaModelo();
                ViewBag.Cantidad = ConseguirCantidades();
                List<ProgramaImagen> listaReal = new List<ProgramaImagen>();

                listaReal = prodMod.CargarProgramasConImagen();

                return View(listaReal);

            }
            catch (Exception)
            {
                //Catch error logs
                return View();
            }

        }



        public IActionResult Product(int id)
        {
            try
            {
                ProductoModelo prodMod = new ProductoModelo();
                ProductoImagen producto = new ProductoImagen();
                ViewBag.Cantidad = ConseguirCantidades();

                producto = prodMod.CargarProductoImagenPorId(id);

                return View(producto);
            }
            catch (Exception)
            {

                throw;
            }

        }


        public IActionResult Workshop(int id)
        {
            try
            {
                WorkshopModelo workMod = new WorkshopModelo();
                WorkshopImagen workshop = new WorkshopImagen();
                ViewBag.Cantidad = ConseguirCantidades();

                workshop = workMod.CargarWorkshopImagenPorId(id);

                return View(workshop);
            }
            catch (Exception)
            {

                throw;
            }

        }


        public IActionResult Actividad(int id)
        {
            try
            {
                ActividadModelo actMod = new ActividadModelo();
                Actividade actividad = new Actividade();
                ViewBag.Cantidad = ConseguirCantidades();

                actividad = actMod.CargarActividadPorId(id);

                return View(actividad);
            }
            catch (Exception)
            {

                throw;
            }

        }


        public IActionResult Programa(int id)
        {
            try
            {
                ProgramaModelo proMod = new ProgramaModelo();
                ProgramaImagen programa = new ProgramaImagen();
                ViewBag.Cantidad = ConseguirCantidades();

                programa = proMod.CargarProgramaImagenPorId(id);

                return View(programa);
            }
            catch (Exception)
            {

                throw;
            }

        }

        [HttpPost]
        public List<Item> CargarListaItems() {

            try
            {

                List<Item> listaItems = SessionHelper.GetObjectFromJson<List<Item>>(HttpContext.Session, "listaItems");

                if (null != listaItems)
                {
                    return listaItems;

                } else
                {
                    return new List<Item>();
                }


            }
            catch (Exception)
            {
                return null;
            }

        }


        public List<SelectListItem> ConseguirCantidades()
        {
            List<SelectListItem> lista = new List<SelectListItem>();

            lista.Add(new SelectListItem { Value = "1", Text = "1" });
            lista.Add(new SelectListItem { Value = "2", Text = "2" });
            lista.Add(new SelectListItem { Value = "3", Text = "3" });
            lista.Add(new SelectListItem { Value = "4", Text = "4" });
            lista.Add(new SelectListItem { Value = "5", Text = "5" });
            lista.Add(new SelectListItem { Value = "6", Text = "6" });
            lista.Add(new SelectListItem { Value = "7", Text = "7" });
            lista.Add(new SelectListItem { Value = "8", Text = "8" });

            return lista;
        }


        public IActionResult AddCart(int? id, string? tipo)
        {
            ProductoModelo prodMod = new ProductoModelo();
            ActividadModelo actMod = new ActividadModelo();
            WorkshopModelo workMod = new WorkshopModelo();
            ProgramaModelo proMod = new ProgramaModelo();

            if (id != null && tipo != null)
            {
                List<Item> codigoItems;
                Item item = new Item();

                if (SessionHelper.GetObjectFromJson<List<Item>>(HttpContext.Session, "listaItems") == null)
                {
                    codigoItems = new List<Item>();
                }
                else
                {
                    codigoItems = SessionHelper.GetObjectFromJson<List<Item>>(HttpContext.Session, "listaItems");
                }

                item.IdItem = id.Value;
                item.TipoItem = tipo;
                item.Cantidad = 1;
                switch (item.TipoItem)
                {

                    case "Product":

                        ProductoImagen producto = prodMod.CargarProductoImagenPorId(item.IdItem);

                        item.Nombre = producto.NombreProducto;
                        item.Precio = producto.Precio;

                        break;

                    case "Workshop":

                        Workshop workshop = workMod.CargarWorkshopPorId(item.IdItem);

                        item.Nombre = workshop.NombreWorkshop;
                        item.Precio = workshop.Precio;

                        break;

                    case "Actividad":

                        Actividade actividad = actMod.CargarActividadPorId(item.IdItem);

                        item.Nombre = actividad.NombreActividades;
                        item.Precio = actividad.Precio;

                        break;

                    case "Programa":

                        Programa programa = proMod.CargarProgramaPorId(item.IdItem);

                        item.Nombre = programa.NombrePrograma;
                        item.Precio = programa.Precio;

                        break;

                    default:
                        break;

                }

                codigoItems.Add(item);
                SessionHelper.SetObjectAsJson(HttpContext.Session, "listaItems", codigoItems);

                ViewBag.Mensaje = "Productos en el carrito: " + codigoItems.Count();

                ViewBag.Carrito = SessionHelper.GetObjectFromJson<List<Item>>(HttpContext.Session, "listaItems");

                return RedirectToAction("Cart", "Shop");
            }

            return RedirectToAction("Cart", "Shop");

        }

        [HttpPost]
        public IActionResult AddCart(int? id, string? tipo, decimal? precio, int? cantidad)
        {
            ProductoModelo prodMod = new ProductoModelo();
            ActividadModelo actMod = new ActividadModelo();
            WorkshopModelo workMod = new WorkshopModelo();
            ProgramaModelo proMod = new ProgramaModelo();

            if (id != null && tipo != null)
            {
                List<Item> codigoItems;
                Item item = new Item();

                if (SessionHelper.GetObjectFromJson<List<Item>>(HttpContext.Session, "listaItems") == null)
                {
                    codigoItems = new List<Item>();
                }
                else
                {
                    codigoItems = SessionHelper.GetObjectFromJson<List<Item>>(HttpContext.Session, "listaItems");
                }

                item.IdItem = id.Value;
                item.TipoItem = tipo;
                item.Cantidad = cantidad.Value;
                switch (item.TipoItem)
                {

                    case "Product":

                        ProductoImagen producto = prodMod.CargarProductoImagenPorId(item.IdItem);

                        item.Nombre = producto.NombreProducto;
                        item.Precio = producto.Precio;

                        break;

                    case "Workshop":

                        Workshop workshop = workMod.CargarWorkshopPorId(item.IdItem);

                        item.Nombre = workshop.NombreWorkshop;
                        item.Precio = workshop.Precio;

                        break;

                    case "Actividad":

                        Actividade actividad = actMod.CargarActividadPorId(item.IdItem);

                        item.Nombre = actividad.NombreActividades;
                        item.Precio = actividad.Precio;

                        break;

                    case "Programa":

                        Programa programa = proMod.CargarProgramaPorId(item.IdItem);

                        item.Nombre = programa.NombrePrograma;
                        item.Precio = programa.Precio;

                        break;

                    default:
                        break;

                }

                codigoItems.Add(item);
                SessionHelper.SetObjectAsJson(HttpContext.Session, "listaItems", codigoItems);

                ViewBag.Mensaje = "Productos en el carrito: " + codigoItems.Count();

                ViewBag.Carrito = SessionHelper.GetObjectFromJson<List<Item>>(HttpContext.Session, "listaItems");

                return RedirectToAction("Cart", "Shop");
            }

            return RedirectToAction("Cart", "Shop");

        }

        public IActionResult Cart()
        {
            List<Item> codigoItems = new List<Item>();

            if(null!= SessionHelper.GetObjectFromJson<List<Item>>(HttpContext.Session, "listaItems"))
            {
                codigoItems = SessionHelper.GetObjectFromJson<List<Item>>(HttpContext.Session, "listaItems");
            }
            return View(codigoItems);
            
        }


        public IActionResult RemoveItem(int? id) {

            List<Item> codigoItems = new List<Item>();

            if (SessionHelper.GetObjectFromJson<List<Item>>(HttpContext.Session, "listaItems") == null)
            {
                return RedirectToAction("Shop", "Shop");
            }
            else
            {
                codigoItems = SessionHelper.GetObjectFromJson<List<Item>>(HttpContext.Session, "listaItems");
                codigoItems.RemoveAll(item => item.IdItem == id.Value);
                SessionHelper.SetObjectAsJson(HttpContext.Session, "listaItems", codigoItems);


                if (codigoItems.Count() == 0)
                {
                    SessionHelper.SetObjectAsJson(HttpContext.Session, "listaItems", new List<Item>());
                    return RedirectToAction("Shop", "Shop");
                }
            }
            return RedirectToAction("Cart", "Shop");
        }


        public IActionResult RegistrarFactura(Factura factura)
        {
                ShopModel model = new ShopModel();

            
            factura.IdUsuario = long.Parse(HttpContext.Session.GetString("id"));
            if (factura.NombreUsuario != null)
                {

                    model.RegistrarFactura(factura);

                    return RedirectToAction("CheckoutValid", "Shop");
                }
                else
                {

                    return RedirectToAction("CheckoutError", "Shop");
                }
        }

        public IActionResult Checkout()
        {
            return View();
        }

        public IActionResult CheckoutValid()
        {
            return View();
        }

        public IActionResult CheckoutError()
        {
            return View();
        }
    }
}
