﻿using FrontEndVakyria.ActionFilters;
using FrontEndVakyria.Modelos_BD;
using FrontEndVakyria.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using FrontEndVakyria.Entities;
using Microsoft.AspNetCore.Http;

namespace FrontEndVakyria.Controllers
{
    [VerificarSesion]
    public class AdminProductoController : Controller
    {

        LogModelo logMod = new LogModelo();
        Log log = new Log();
        static string mensajeError = "";
        static bool itFailed = false;
        

        public void RegistrarLog(String descripcion, String modulo, String accion)
        {
            try
            {
                log.Fecha = DateTime.Now;
                log.Descripcion = descripcion;
                log.Modulo = modulo;
                log.Accion = accion;
                if (null == ViewBag.User)
                {
                    log.Usuario = "Invitado";
                }
                else
                {
                    log.Usuario = ViewBag.User;
                }
                logMod.IngresarLog(log);
            }
            catch (Exception e)
            {
               
            }

        }

        public List<SelectListItem> ConseguirLista()
        {
            List<SelectListItem> lista = new List<SelectListItem>();

            lista.Add(new SelectListItem { Value = "--Porfavor Seleccionar--", Text = "--Porfavor Seleccionar--" });
            lista.Add(new SelectListItem { Value = "Ropa Deportiva", Text = "Ropa Deportiva" });
            lista.Add(new SelectListItem { Value = "Suplementos", Text = "Suplementos" });
            lista.Add(new SelectListItem { Value = "Accesorios", Text = "Accesorios" });

            return lista;
        }

        [HttpPost]
        public IActionResult InsertarProducto(Producto producto)
        {
            try
            {
                ProductoModelo prodMod = new ProductoModelo();
                prodMod.IngresarProducto(producto);

                RegistrarLog("Se ingresó el producto: "+producto.NombreProducto, "AdminProducto", "InsertarProducto");

                return RedirectToAction("Consultar", "AdminProducto");
            }
            catch (Exception e)
            {
                RegistrarLog(e.ToString().Substring(0,299), "AdminProducto", "InsertarProducto");
                return RedirectToAction("Consultar", "AdminProducto");
            }

        }


        [HttpGet]
        public IActionResult Insertar()
        {
            ViewBag.Imagenes = CargarImagenes();
            ViewBag.TipoProducto = ConseguirLista();
            return View();
        }

        [HttpGet]
        public IActionResult Consultar()
        {
            try
            {
                if (mensajeError != null && mensajeError != "" && itFailed == false)
                {
                    itFailed = true;
                    ViewBag.Mensaje = mensajeError;
                }
                else if(itFailed==true && mensajeError!="")
                {
                    itFailed = false;
                    mensajeError = "";
                }
                
                ViewBag.TipoProducto = ConseguirLista();
                ProductoModelo prodMod = new ProductoModelo();
                return View(prodMod.CargarProductos());

            }
            catch (Exception e)
            {

                RegistrarLog(e.ToString().Substring(0, 299), "AdminProducto", "Consultar");

                return View();
            }

        }


        [HttpPost]
        public String ConsultarPorId(int IdProducto)
        {
            try
            {
                ProductoModelo prodMod = new ProductoModelo();
                Producto producto = new Producto();
                producto = prodMod.CargarProductoPorId(IdProducto);

                if (null != producto)
                {
                    return JsonConvert.SerializeObject(producto);
                }
                else
                {
                    return JsonConvert.SerializeObject(null);
                }

            }
            catch (Exception e)
            {
                RegistrarLog(e.InnerException.ToString(), "AdminProducto", "ConsultarPorId");

                return JsonConvert.SerializeObject(null);
            }

        }

        [HttpGet]
        public IActionResult Actualizar()
        {
            try
            {
                ProductoModelo prodMod = new ProductoModelo();
                var listaProductos = prodMod.CargarProductos();

                List<SelectListItem> ListaCombo = new List<SelectListItem>();
                foreach (var item in listaProductos)
                {
                    ListaCombo.Add(new SelectListItem
                    {
                        Value = item.IdProducto.ToString(),
                        Text = item.NombreProducto
                    });
                }

                ViewBag.Imagenes = CargarImagenes();
                ViewBag.ListaProductos = ListaCombo.ToList();
                ViewBag.TipoProducto = ConseguirLista();
                return View();
            }
            catch (Exception e)
            {
                RegistrarLog(e.ToString().Substring(0, 299), "AdminProducto", "Actualizar");

                return View();
            }

        }

        public List<SelectListItem> CargarImagenes()
        {
            try
            {
                ImagenModelo imMod = new ImagenModelo();
                var listaImagenes = imMod.CargarImagenes();

                List<SelectListItem> ListaCombo = new List<SelectListItem>();

                foreach (var item in listaImagenes)
                {
                    ListaCombo.Add(new SelectListItem
                    {
                        Value = item.IdImagen.ToString(),
                        Text = item.NombreImagen
                    });
                }

                ViewBag.ListaProductos = ListaCombo.ToList();

                return ListaCombo;
            }
            catch (Exception e)
            {

                RegistrarLog(e.ToString().Substring(0, 299), "AdminProducto", "CargarImagenes");

                return new List<SelectListItem>(); ;
            }

        }



        [HttpPost]
        public IActionResult ActualizarProducto(Producto producto)
        {
            try
            {
                ProductoModelo prodMod = new ProductoModelo();
                int rowsAfectados = prodMod.ModificarProducto(producto);

                if (rowsAfectados > 0)
                {
                    RegistrarLog("Se actualizó el producto: " + producto.IdProducto.ToString(), "AdminProducto", "ActualizarProducto");
                    return RedirectToAction("Consultar", "AdminProducto"); //Actualizo un registro vamos a Consulta
                }
                else
                {
                    return RedirectToAction("Actualizar", "AdminProducto"); //Si hubo problema, volver al view
                }
            }
            catch (Exception e)
            {

                RegistrarLog(e.InnerException.ToString(), "AdminProducto", "ActualizarProducto");

                return RedirectToAction("Consultar", "AdminProducto");
            }

        }

        [HttpGet]
        public IActionResult Eliminar()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Imagen()
        {
            try
            {
                ImagenModelo imMod = new ImagenModelo();
                var listaImagenes = imMod.CargarImagenes();

                List<TipoImagen> listaReal = new List<TipoImagen>();

                foreach (var item in listaImagenes)
                {
                    listaReal.Add(new TipoImagen
                    {
                        IdImagen = item.IdImagen,
                        NombreImagen = item.NombreImagen,
                        DatosImagen = item.DatosImagen,
                        imgDataURL = string.Format("data:image/png;base64,{0}", Convert.ToBase64String(item.DatosImagen))


                    });
                }

                return View(listaReal);
            }
            catch (Exception e)
            {
                RegistrarLog(e.InnerException.ToString(), "AdminProducto", "Imagen");

                return RedirectToAction("Consultar", "AdminProducto");
            }


        }



        [HttpPost]
        public String EliminarImagen(int IdImagen)
        {
            try
            {
                ImagenModelo imgMod = new ImagenModelo();
                int rowsAfectados = imgMod.EliminarImagen(IdImagen);

                if (rowsAfectados > 0)
                {
                    RegistrarLog("Se eliminó la imagen: " + IdImagen, "AdminProducto", "EliminarImagen");
                    return JsonConvert.SerializeObject(rowsAfectados);
                }
                else
                {
                    return JsonConvert.SerializeObject(null);
                }
            }
            catch (Exception e)
            {
                RegistrarLog(e.ToString().Substring(0, 299), "AdminProducto", "EliminarImagen");

                return JsonConvert.SerializeObject(null);
            }

        }

        [HttpPost]
        public String EliminarProducto(int IdProd)
        {
            try
            {
                ProductoModelo prodMod = new ProductoModelo();
                int rowsAfectados = prodMod.EliminarProducto(IdProd);

                if (rowsAfectados > 0) { 
                    RegistrarLog("Se eliminó el producto: " + IdProd, "AdminProducto", "EliminarProducto");
                    return JsonConvert.SerializeObject(rowsAfectados);
                }
                else
                {
                    return JsonConvert.SerializeObject(null);
                }
            }
            catch (Exception e)
            {
                if (e.InnerException.Message.Contains("a foreign key constraint fails"))
                {
                    RegistrarLog(e.ToString().Substring(0, 299), "AdminProducto", "EliminarProducto");
                    mensajeError = "El producto no se pudo eliminar porque ya ha sido adquirido por nuestros clientes";
                    itFailed = false;
                    return JsonConvert.SerializeObject(null);
                }
                RegistrarLog(e.ToString().Substring(0, 299), "AdminProducto", "EliminarProducto");
                return JsonConvert.SerializeObject(null);
            }

        }

        public IActionResult ReportesProductos()
        {

            try
            {
                ViewBag.TipoProducto = ConseguirLista();
                ProductoModelo prodMod = new ProductoModelo();



                var totalVentas = prodMod.totalVentas();
                HttpContext.Session.SetString("totalVentas", totalVentas.ToString());

                var totalVentasSemana = prodMod.totalVentasSemana();
                HttpContext.Session.SetString("totalVentasSemana", totalVentasSemana.ToString());

                var totalRopaDeportiva = prodMod.totalVentasProductosDeportivos();
                HttpContext.Session.SetString("totalRopaDeportiva", totalRopaDeportiva.ToString());

                var totalVentasSuplementos = prodMod.totalVentasSuplementos();
                HttpContext.Session.SetString("totalVentasSuplementos", totalVentasSuplementos.ToString());

                var totalVentasAccesorios = prodMod.totalVentasAccesorios();
                HttpContext.Session.SetString("totalVentasAccesorios", totalVentasAccesorios.ToString());


                var cantProductos = prodMod.cantProductos();
                HttpContext.Session.SetString("cantProductos", cantProductos.ToString());

                var totalOrdenes = prodMod.totalOrdenes();
                HttpContext.Session.SetString("totalOrdenes", totalOrdenes.ToString());

             

                ViewBag.TipoActividad = ConseguirLista();


                return View(prodMod.CargarProductos());

            }
            catch (Exception e)
            {

                RegistrarLog(e.ToString().Substring(0, 299), "AdminProducto", "ReportesProductos");
                return View();
            }
            
        }
    

    }
}
