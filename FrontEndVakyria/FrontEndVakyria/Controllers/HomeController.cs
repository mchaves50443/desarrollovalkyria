﻿using FrontEndVakyria.Modelos_BD;
using FrontEndVakyria.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEndVakyria.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        Log log = new Log();
        LogModelo logMod = new LogModelo();

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        //de  la plantilla
        public IActionResult Home()
        {
            return View();
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public IActionResult InsertarConsulta(Consulta consulta)
        {
            try
            {
               
                ConsultaModelo conMod = new ConsultaModelo();
                consulta.FechaIngresada = DateTime.Now;
                conMod.IngresarConsulta(consulta);

                RegistrarLog("Nueva consulta " + consulta.NombrePersona, "Consultas", "InsertarConsulta");

                return RedirectToAction("Home", "Home");
            }
            catch (Exception e)
            {
                RegistrarLog(e.ToString().Substring(0, 299), "Consultas", "InsertarConsulta");
                return RedirectToAction("Contact", "Home");
            }
        }

        public void RegistrarLog(String descripcion, String modulo, String accion)
        {
            try
            {
                log.Fecha = DateTime.Now;
                log.Descripcion = descripcion;
                log.Modulo = modulo;
                log.Accion = accion;
                if (null == ViewBag.User)
                {
                    log.Usuario = "Invitado";
                }
                else
                {
                    log.Usuario = ViewBag.User;
                }
                logMod.IngresarLog(log);
            }
            catch (Exception e)
            {

            }

        }
    }
}
