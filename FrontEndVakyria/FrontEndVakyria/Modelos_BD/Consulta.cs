﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FrontEndVakyria.Modelos_BD
{
    public partial class Consulta
    {
        public long IdConsulta { get; set; }
        public string NombrePersona { get; set; }
        public string Email { get; set; }
        public string Consulta1 { get; set; }
        public string Mensaje { get; set; }
        public DateTime FechaIngresada { get; set; }
    }
}
