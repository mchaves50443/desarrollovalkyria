﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FrontEndVakyria.Modelos_BD
{
    public partial class Usuario
    {
        public Usuario()
        {
            Facturas = new HashSet<Factura>();
        }

        public long IdUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public string PrimerApellidoUsuario { get; set; }
        public string SegundoApellidoUsuario { get; set; }
        public string CedulaUsuario { get; set; }
        public string CorreoElectronico { get; set; }
        public string TelefonoUsuario { get; set; }
        public string ContrasenaUsuario { get; set; }
        public short RolUsuario { get; set; }
        public DateTime? UltimaConexion { get; set; }

        public virtual ICollection<Factura> Facturas { get; set; }
    }
}
