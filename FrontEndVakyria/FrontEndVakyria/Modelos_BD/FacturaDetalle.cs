﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FrontEndVakyria.Modelos_BD
{
    public partial class FacturaDetalle
    {
        public int IdFacturaDetalle { get; set; }
        public long? IdFactura { get; set; }
        public long? IdPrograma { get; set; }
        public int? IdProducto { get; set; }
        public int? IdWorkshop { get; set; }
        public long? IdActividad { get; set; }
        public int? Cantidad { get; set; }
        public decimal? Precio { get; set; }

        public virtual Actividade IdActividadNavigation { get; set; }
        public virtual Factura IdFacturaNavigation { get; set; }
        public virtual Producto IdProductoNavigation { get; set; }
        public virtual Programa IdProgramaNavigation { get; set; }
        public virtual Workshop IdWorkshopNavigation { get; set; }
    }
}
