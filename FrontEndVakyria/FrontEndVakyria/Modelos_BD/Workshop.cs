﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FrontEndVakyria.Modelos_BD
{
    public partial class Workshop
    {
        public Workshop()
        {
            FacturaDetalles = new HashSet<FacturaDetalle>();
        }

        public int IdWorkshop { get; set; }
        public string NombreWorkshop { get; set; }
        public int MinInventario { get; set; }
        public int MaxInventatio { get; set; }
        public decimal Precio { get; set; }
        public DateTime Fecha { get; set; }
        public string Descripcion { get; set; }
        public int? IdImagen { get; set; }
        public int? CuposVendidos { get; set; }

        public virtual Imagen IdImagenNavigation { get; set; }
        public virtual ICollection<FacturaDetalle> FacturaDetalles { get; set; }
    }
}
