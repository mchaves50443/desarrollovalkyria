﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FrontEndVakyria.Modelos_BD
{
    public partial class Programa
    {
        public Programa()
        {
            FacturaDetalles = new HashSet<FacturaDetalle>();
        }

        public long IdPrograma { get; set; }
        public string NombrePrograma { get; set; }
        public decimal Precio { get; set; }
        public string Descripcion { get; set; }
        public int IdImagen { get; set; }
        public int? CantVendidas { get; set; }

        public virtual Imagen IdImagenNavigation { get; set; }
        public virtual ICollection<FacturaDetalle> FacturaDetalles { get; set; }
    }
}
