﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FrontEndVakyria.Modelos_BD
{
    public partial class Archivo
    {
        public int IdArchivo { get; set; }
        public string NombreArchivo { get; set; }
        public byte[] DatosArchivo { get; set; }
    }
}
