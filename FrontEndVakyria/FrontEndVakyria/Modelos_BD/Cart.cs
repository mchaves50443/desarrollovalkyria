﻿using FrontEndVakyria.Entities;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FrontEndVakyria.Modelos_BD
{
    public class CarritoCompraModel
    {

        public List<ProductoImagen> ConsultarProducto()
        {
            List<ProductoImagen> listaProgramas = new List<ProductoImagen>();

            using (var db = new BD_ProyectoValkyriaContext())
            {
                var resultado = (from tp in db.Productos
                                 select new { tp }).ToList();

                foreach (var item in resultado)
                {
                    listaProgramas.Add(new ProductoImagen
                    {
                        IdProducto = item.tp.IdProducto,
                        IdImagen = item.tp.IdImagen,
                        NombreProducto = item.tp.NombreProducto,
                        Precio = (decimal)item.tp.Precio
                    }); ;
                }
            }

            return listaProgramas;
        }


        public List<ProductoImagen> BuscarProducto(List<long> id)
        {
            List<ProductoImagen> listaProgramas = new List<ProductoImagen>();

            using (var db = new BD_ProyectoValkyriaContext())
            {

                var resultado = (from tp in db.Productos
                                 where id.Contains(tp.IdProducto)
                                 select new { tp }).ToList();

                foreach (var item in resultado)
                {
                    listaProgramas.Add(new ProductoImagen
                    {
                        IdProducto = item.tp.IdProducto,
                        IdImagen = item.tp.IdImagen,
                        NombreProducto = item.tp.NombreProducto,
                        Precio = (decimal)item.tp.Precio
                    });
                }
            }

            return listaProgramas;
        }


        public List<WorkshopImagen> ConsultarWorkshop()
        {
            List<WorkshopImagen> listaWorkshops = new List<WorkshopImagen>();

            using (var db = new BD_ProyectoValkyriaContext())
            {
                var resultado = (from tp in db.Workshops
                                 select new { tp }).ToList();

                foreach (var item in resultado)
                {
                    listaWorkshops.Add(new WorkshopImagen
                    {
                        IdWorkshop = item.tp.IdWorkshop,
                        IdImagen = item.tp.IdImagen,
                        NombreWorkshop = item.tp.NombreWorkshop,
                        Precio = (decimal)item.tp.Precio
                    }); ;
                }
            }

            return listaWorkshops;
        }


        public List<WorkshopImagen> BuscarWorkshop(List<long> id)
        {
            List<WorkshopImagen> listaWorkshops = new List<WorkshopImagen>();

            using (var db = new BD_ProyectoValkyriaContext())
            {

                var resultado = (from tp in db.Workshops
                                 where id.Contains(tp.IdWorkshop)
                                 select new { tp }).ToList();

                foreach (var item in resultado)
                {
                    listaWorkshops.Add(new WorkshopImagen
                    {
                        IdWorkshop = item.tp.IdWorkshop,
                        IdImagen = item.tp.IdImagen,
                        NombreWorkshop = item.tp.NombreWorkshop,
                        Precio = (decimal)item.tp.Precio
                    });
                }
            }

            return listaWorkshops;
        }





        //public bool RegistrarVenta(long id_usuario, decimal total, DateTime fechaActual, string nombre,
        //    string apellidos, string correo, string telefono, string provincia, string canton, string distrito,
        //    string direccion, string zip, string pais, string codigo)
        //{
        //    using (var contextoBD = new ProyectoPrograAvanzadaEntities())
        //    {
        //        contextoBD.RegistrarVenta(id_usuario, total, fechaActual, nombre, apellidos, correo, telefono,
        //            provincia, canton, distrito, direccion, zip, pais, codigo);

        //        return true;
        //    }

        //    //return false if not registered
        //    return false;
        //}


    }
}
