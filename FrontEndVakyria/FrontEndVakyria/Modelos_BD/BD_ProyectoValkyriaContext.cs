﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace FrontEndVakyria.Modelos_BD
{
    public partial class BD_ProyectoValkyriaContext : DbContext
    {
        public BD_ProyectoValkyriaContext()
        {
        }

        public BD_ProyectoValkyriaContext(DbContextOptions<BD_ProyectoValkyriaContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Actividade> Actividades { get; set; }
        public virtual DbSet<Archivo> Archivos { get; set; }
        public virtual DbSet<Consulta> Consultas { get; set; }
        public virtual DbSet<Factura> Facturas { get; set; }
        public virtual DbSet<FacturaDetalle> FacturaDetalles { get; set; }
        public virtual DbSet<Imagen> Imagens { get; set; }
        public virtual DbSet<Log> Logs { get; set; }
        public virtual DbSet<Producto> Productos { get; set; }
        public virtual DbSet<Programa> Programas { get; set; }
        public virtual DbSet<Usuario> Usuarios { get; set; }
        public virtual DbSet<Workshop> Workshops { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseMySql("server=127.0.0.1;port=3306;database=BD_ProyectoValkyria;uid=admin-dev;pwd=Admin.2021", Microsoft.EntityFrameworkCore.ServerVersion.FromString("8.0.28-mysql"), x => x.UseNetTopologySuite());
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Actividade>(entity =>
            {
                entity.HasKey(e => e.IdActividades)
                    .HasName("PRIMARY");

                entity.Property(e => e.IdActividades).HasColumnName("idActividades");

                entity.Property(e => e.CuposVendidos).HasColumnName("cuposVendidos");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnType("varchar(255)")
                    .HasColumnName("descripcion")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Fecha)
                    .HasColumnType("datetime")
                    .HasColumnName("fecha");

                entity.Property(e => e.MaxInventario).HasColumnName("maxInventario");

                entity.Property(e => e.MinInventario).HasColumnName("minInventario");

                entity.Property(e => e.NombreActividades)
                    .IsRequired()
                    .HasColumnType("varchar(225)")
                    .HasColumnName("nombreActividades")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Precio)
                    .HasPrecision(10)
                    .HasColumnName("precio");

                entity.Property(e => e.TipoActividad).HasColumnName("tipoActividad");
            });

            modelBuilder.Entity<Archivo>(entity =>
            {
                entity.HasKey(e => e.IdArchivo)
                    .HasName("PRIMARY");

                entity.ToTable("Archivo");

                entity.Property(e => e.IdArchivo).HasColumnName("idArchivo");

                entity.Property(e => e.DatosArchivo)
                    .IsRequired()
                    .HasColumnType("mediumblob")
                    .HasColumnName("datosArchivo");

                entity.Property(e => e.NombreArchivo)
                    .IsRequired()
                    .HasColumnType("varchar(100)")
                    .HasColumnName("nombreArchivo")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");
            });

            modelBuilder.Entity<Consulta>(entity =>
            {
                entity.HasKey(e => e.IdConsulta)
                    .HasName("PRIMARY");

                entity.Property(e => e.IdConsulta).HasColumnName("idConsulta");

                entity.Property(e => e.Consulta1)
                    .IsRequired()
                    .HasColumnType("varchar(50)")
                    .HasColumnName("consulta")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnType("varchar(50)")
                    .HasColumnName("email")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.FechaIngresada)
                    .HasColumnType("datetime")
                    .HasColumnName("fechaIngresada");

                entity.Property(e => e.Mensaje)
                    .IsRequired()
                    .HasColumnType("varchar(300)")
                    .HasColumnName("mensaje")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.NombrePersona)
                    .IsRequired()
                    .HasColumnType("varchar(225)")
                    .HasColumnName("nombrePersona")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");
            });

            modelBuilder.Entity<Factura>(entity =>
            {
                entity.HasKey(e => e.IdFactura)
                    .HasName("PRIMARY");

                entity.ToTable("Factura");

                entity.HasIndex(e => e.IdUsuario, "idUsuario_idx");

                entity.Property(e => e.IdFactura).HasColumnName("idFactura");

                entity.Property(e => e.ApellidosUsuario)
                    .IsRequired()
                    .HasColumnType("varchar(45)")
                    .HasColumnName("apellidosUsuario")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Canton)
                    .IsRequired()
                    .HasColumnType("varchar(45)")
                    .HasColumnName("canton")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.CodigoPostal).HasColumnName("codigoPostal");

                entity.Property(e => e.Correo)
                    .IsRequired()
                    .HasColumnType("varchar(45)")
                    .HasColumnName("correo")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Direccion)
                    .IsRequired()
                    .HasColumnType("varchar(150)")
                    .HasColumnName("direccion")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Distrito)
                    .IsRequired()
                    .HasColumnType("varchar(45)")
                    .HasColumnName("distrito")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Estado)
                    .HasColumnType("tinyint(3) unsigned zerofill")
                    .HasColumnName("estado")
                    .HasDefaultValueSql("'000'");

                entity.Property(e => e.Fecha)
                    .HasColumnType("datetime")
                    .HasColumnName("fecha");

                entity.Property(e => e.IdUsuario).HasColumnName("idUsuario");

                entity.Property(e => e.NombreUsuario)
                    .IsRequired()
                    .HasColumnType("varchar(45)")
                    .HasColumnName("nombreUsuario")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Provincia)
                    .IsRequired()
                    .HasColumnType("varchar(45)")
                    .HasColumnName("provincia")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Telefono)
                    .IsRequired()
                    .HasColumnType("varchar(45)")
                    .HasColumnName("telefono")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Total)
                    .HasPrecision(10)
                    .HasColumnName("total");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.Facturas)
                    .HasForeignKey(d => d.IdUsuario)
                    .HasConstraintName("idUsuario");
            });

            modelBuilder.Entity<FacturaDetalle>(entity =>
            {
                entity.HasKey(e => e.IdFacturaDetalle)
                    .HasName("PRIMARY");

                entity.ToTable("FacturaDetalle");

                entity.HasIndex(e => e.IdActividad, "idActividad_idx");

                entity.HasIndex(e => e.IdFacturaDetalle, "idFacturaDetalle_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.IdFactura, "idFactura_idx");

                entity.HasIndex(e => e.IdProducto, "idProducto_idx");

                entity.HasIndex(e => e.IdPrograma, "idPrograma_idx");

                entity.HasIndex(e => e.IdWorkshop, "idWorkshop_idx");

                entity.Property(e => e.IdFacturaDetalle).HasColumnName("idFacturaDetalle");

                entity.Property(e => e.Cantidad).HasColumnName("cantidad");

                entity.Property(e => e.IdActividad).HasColumnName("idActividad");

                entity.Property(e => e.IdFactura).HasColumnName("idFactura");

                entity.Property(e => e.IdProducto).HasColumnName("idProducto");

                entity.Property(e => e.IdPrograma).HasColumnName("idPrograma");

                entity.Property(e => e.IdWorkshop).HasColumnName("idWorkshop");

                entity.Property(e => e.Precio)
                    .HasPrecision(10)
                    .HasColumnName("precio");

                entity.HasOne(d => d.IdActividadNavigation)
                    .WithMany(p => p.FacturaDetalles)
                    .HasForeignKey(d => d.IdActividad)
                    .HasConstraintName("idActividad");

                entity.HasOne(d => d.IdFacturaNavigation)
                    .WithMany(p => p.FacturaDetalles)
                    .HasForeignKey(d => d.IdFactura)
                    .HasConstraintName("idFactura");

                entity.HasOne(d => d.IdProductoNavigation)
                    .WithMany(p => p.FacturaDetalles)
                    .HasForeignKey(d => d.IdProducto)
                    .HasConstraintName("idProducto");

                entity.HasOne(d => d.IdProgramaNavigation)
                    .WithMany(p => p.FacturaDetalles)
                    .HasForeignKey(d => d.IdPrograma)
                    .HasConstraintName("idPrograma");

                entity.HasOne(d => d.IdWorkshopNavigation)
                    .WithMany(p => p.FacturaDetalles)
                    .HasForeignKey(d => d.IdWorkshop)
                    .HasConstraintName("idWorkshop");
            });

            modelBuilder.Entity<Imagen>(entity =>
            {
                entity.HasKey(e => e.IdImagen)
                    .HasName("PRIMARY");

                entity.ToTable("Imagen");

                entity.HasComment("Tabla para mantener las imagenes de cada producto");

                entity.HasIndex(e => e.IdImagen, "idImagen_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.IdImagen).HasColumnName("idImagen");

                entity.Property(e => e.DatosImagen)
                    .IsRequired()
                    .HasColumnType("mediumblob")
                    .HasColumnName("datosImagen");

                entity.Property(e => e.NombreImagen)
                    .IsRequired()
                    .HasColumnType("varchar(100)")
                    .HasColumnName("nombreImagen")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");
            });

            modelBuilder.Entity<Log>(entity =>
            {
                entity.HasKey(e => e.IdLog)
                    .HasName("PRIMARY");

                entity.ToTable("Log");

                entity.Property(e => e.IdLog).HasColumnName("idLog");

                entity.Property(e => e.Accion)
                    .IsRequired()
                    .HasColumnType("varchar(45)")
                    .HasColumnName("accion")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnType("varchar(300)")
                    .HasColumnName("descripcion")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Fecha)
                    .HasColumnType("date")
                    .HasColumnName("fecha");

                entity.Property(e => e.Modulo)
                    .IsRequired()
                    .HasColumnType("varchar(45)")
                    .HasColumnName("modulo")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Usuario)
                    .IsRequired()
                    .HasColumnType("varchar(45)")
                    .HasColumnName("usuario")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");
            });

            modelBuilder.Entity<Producto>(entity =>
            {
                entity.HasKey(e => e.IdProducto)
                    .HasName("PRIMARY");

                entity.ToTable("Producto");

                entity.HasIndex(e => e.IdImagen, "idImagen_idx");

                entity.HasIndex(e => e.IdProducto, "idProducto_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.IdProducto).HasColumnName("idProducto");

                entity.Property(e => e.CantInventario).HasColumnName("cantInventario");

                entity.Property(e => e.CantVendidas).HasColumnName("cantVendidas");

                entity.Property(e => e.Descripcion)
                    .HasColumnType("varchar(150)")
                    .HasColumnName("descripcion")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.IdImagen).HasColumnName("idImagen");

                entity.Property(e => e.NombreProducto)
                    .IsRequired()
                    .HasColumnType("varchar(50)")
                    .HasColumnName("nombreProducto")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Precio)
                    .HasPrecision(10)
                    .HasColumnName("precio");

                entity.Property(e => e.Talla)
                    .HasColumnType("varchar(15)")
                    .HasColumnName("talla")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.TipoProducto)
                    .IsRequired()
                    .HasColumnType("varchar(50)")
                    .HasColumnName("tipoProducto")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.HasOne(d => d.IdImagenNavigation)
                    .WithMany(p => p.Productos)
                    .HasForeignKey(d => d.IdImagen)
                    .HasConstraintName("idImagen");
            });

            modelBuilder.Entity<Programa>(entity =>
            {
                entity.HasKey(e => e.IdPrograma)
                    .HasName("PRIMARY");

                entity.ToTable("Programa");

                entity.HasIndex(e => e.IdImagen, "idImagen");

                entity.Property(e => e.IdPrograma).HasColumnName("idPrograma");

                entity.Property(e => e.CantVendidas).HasColumnName("cantVendidas");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnType("varchar(255)")
                    .HasColumnName("descripcion")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.IdImagen).HasColumnName("idImagen");

                entity.Property(e => e.NombrePrograma)
                    .IsRequired()
                    .HasColumnType("varchar(225)")
                    .HasColumnName("nombrePrograma")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Precio)
                    .HasPrecision(10)
                    .HasColumnName("precio");

                entity.HasOne(d => d.IdImagenNavigation)
                    .WithMany(p => p.Programas)
                    .HasForeignKey(d => d.IdImagen)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Programa_ibfk_1");
            });

            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.HasKey(e => e.IdUsuario)
                    .HasName("PRIMARY");

                entity.ToTable("Usuario");

                entity.HasIndex(e => e.IdUsuario, "idUsuario_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.IdUsuario).HasColumnName("idUsuario");

                entity.Property(e => e.CedulaUsuario)
                    .IsRequired()
                    .HasColumnType("varchar(15)")
                    .HasColumnName("cedulaUsuario")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.ContrasenaUsuario)
                    .IsRequired()
                    .HasColumnType("varchar(50)")
                    .HasColumnName("contrasenaUsuario")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.CorreoElectronico)
                    .IsRequired()
                    .HasColumnType("varchar(80)")
                    .HasColumnName("correoElectronico")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.NombreUsuario)
                    .IsRequired()
                    .HasColumnType("varchar(30)")
                    .HasColumnName("nombreUsuario")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.PrimerApellidoUsuario)
                    .IsRequired()
                    .HasColumnType("varchar(30)")
                    .HasColumnName("primerApellidoUsuario")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.RolUsuario).HasColumnName("rolUsuario");

                entity.Property(e => e.SegundoApellidoUsuario)
                    .IsRequired()
                    .HasColumnType("varchar(30)")
                    .HasColumnName("segundoApellidoUsuario")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.TelefonoUsuario)
                    .IsRequired()
                    .HasColumnType("varchar(20)")
                    .HasColumnName("telefonoUsuario")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.UltimaConexion)
                    .HasColumnType("timestamp")
                    .HasColumnName("ultimaConexion");
            });

            modelBuilder.Entity<Workshop>(entity =>
            {
                entity.HasKey(e => e.IdWorkshop)
                    .HasName("PRIMARY");

                entity.ToTable("Workshop");

                entity.HasIndex(e => e.IdImagen, "idImagen_idx");

                entity.Property(e => e.IdWorkshop).HasColumnName("idWorkshop");

                entity.Property(e => e.CuposVendidos).HasColumnName("cuposVendidos");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnType("varchar(300)")
                    .HasColumnName("descripcion")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Fecha)
                    .HasColumnType("datetime")
                    .HasColumnName("fecha");

                entity.Property(e => e.IdImagen).HasColumnName("idImagen");

                entity.Property(e => e.MaxInventatio).HasColumnName("maxInventatio");

                entity.Property(e => e.MinInventario).HasColumnName("minInventario");

                entity.Property(e => e.NombreWorkshop)
                    .IsRequired()
                    .HasColumnType("varchar(255)")
                    .HasColumnName("nombreWorkshop")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Precio)
                    .HasPrecision(10)
                    .HasColumnName("precio");

                entity.HasOne(d => d.IdImagenNavigation)
                    .WithMany(p => p.Workshops)
                    .HasForeignKey(d => d.IdImagen)
                    .HasConstraintName("idImagenFK");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
