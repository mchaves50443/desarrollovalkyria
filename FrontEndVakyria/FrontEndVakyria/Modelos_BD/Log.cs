﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FrontEndVakyria.Modelos_BD
{
    public partial class Log
    {
        public int IdLog { get; set; }
        public string Descripcion { get; set; }
        public DateTime Fecha { get; set; }
        public string Modulo { get; set; }
        public string Accion { get; set; }
        public string Usuario { get; set; }
    }
}
