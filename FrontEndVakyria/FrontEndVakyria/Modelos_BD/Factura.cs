﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FrontEndVakyria.Modelos_BD
{
    public partial class Factura
    {
        public Factura()
        {
            FacturaDetalles = new HashSet<FacturaDetalle>();
        }

        public long IdFactura { get; set; }
        public long? IdUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public string ApellidosUsuario { get; set; }
        public string Provincia { get; set; }
        public string Canton { get; set; }
        public string Distrito { get; set; }
        public string Direccion { get; set; }
        public long CodigoPostal { get; set; }
        public string Telefono { get; set; }
        public string Correo { get; set; }
        public decimal Total { get; set; }
        public DateTime Fecha { get; set; }
        public byte? Estado { get; set; }

        public virtual Usuario IdUsuarioNavigation { get; set; }
        public virtual ICollection<FacturaDetalle> FacturaDetalles { get; set; }
    }
}
