﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FrontEndVakyria.Modelos_BD
{
    public partial class Producto
    {
        public Producto()
        {
            FacturaDetalles = new HashSet<FacturaDetalle>();
        }

        public int IdProducto { get; set; }
        public string NombreProducto { get; set; }
        public string Descripcion { get; set; }
        public string TipoProducto { get; set; }
        public int CantInventario { get; set; }
        public decimal Precio { get; set; }
        public int? IdImagen { get; set; }
        public int? CantVendidas { get; set; }
        public string Talla { get; set; }

        public virtual Imagen IdImagenNavigation { get; set; }
        public virtual ICollection<FacturaDetalle> FacturaDetalles { get; set; }
    }
}
