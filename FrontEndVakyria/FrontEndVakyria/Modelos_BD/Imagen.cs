﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FrontEndVakyria.Modelos_BD
{
    public partial class Imagen
    {
        public Imagen()
        {
            Productos = new HashSet<Producto>();
            Programas = new HashSet<Programa>();
            Workshops = new HashSet<Workshop>();
        }

        public int IdImagen { get; set; }
        public string NombreImagen { get; set; }
        public byte[] DatosImagen { get; set; }

        public virtual ICollection<Producto> Productos { get; set; }
        public virtual ICollection<Programa> Programas { get; set; }
        public virtual ICollection<Workshop> Workshops { get; set; }
    }
}
