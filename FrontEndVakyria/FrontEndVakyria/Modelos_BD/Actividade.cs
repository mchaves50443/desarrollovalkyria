﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FrontEndVakyria.Modelos_BD
{
    public partial class Actividade
    {
        public Actividade()
        {
            FacturaDetalles = new HashSet<FacturaDetalle>();
        }

        public long IdActividades { get; set; }
        public string NombreActividades { get; set; }
        public decimal Precio { get; set; }
        public DateTime Fecha { get; set; }
        public string Descripcion { get; set; }
        public short TipoActividad { get; set; }
        public short MinInventario { get; set; }
        public short MaxInventario { get; set; }
        public int? CuposVendidos { get; set; }

        public virtual ICollection<FacturaDetalle> FacturaDetalles { get; set; }
    }
}
