﻿function EliminaConsulta(idCon) {


    $.ajax({
        type: 'POST',
        url: '/Admin/EliminarConsulta',
        data: {
            idCon: idCon
        },
        dataType: 'json',
        success: function (data) {

            window.location.href = './CargarConsultas';
            console.log("Eliminado");

        },
        error: function (data) {
            alert('Hubo un problema, por favor vuelva a intentarlo más tarde');
        }
    });

}