﻿$("#IdProducto").change(function () {

    ConsultaPrecio();

});

function ConsultaPrecio() {
    var IdProducto = $("#IdProducto").val();

    $.ajax({
        type: 'POST',
        url: '/AdminProducto/ConsultarPorId',
        data: {
            IdProducto: IdProducto
        },
        dataType: 'json',
        success: function (data) {

            console.log(data);
            $("#CantInventario").val(data.CantInventario);
            $("#Descripcion").val(data.Descripcion);
            $("#IdImagen").val(data.IdImagen);
            $("#NombreProducto").val(data.NombreProducto);
            $("#Precio").val(data.Precio);
            $("#TipoProducto").val(data.TipoProducto);

        },
        error: function (data) {
            alert('No se pudo cargar el producto');
            $("#TipoProducto").val("No se pudo cargar el producto");
        }
    });

}



//$("#IdProd").click(function () {

//    var IdProd = $(this).attr("value");
    
//    if (confirm("¿Desea eliminar este producto?") == true) {

//        EliminaProducto();
//    }

//});

function EliminaProducto(IdProd) {


    $.ajax({
        type: 'POST',
        url: '/AdminProducto/EliminarProducto',
        data: {
            IdProd: IdProd
        },
        dataType: 'json',
        success: function (data) {

            window.location.href = './Consultar';
            console.log("Eliminado");

        },
        error: function (data) {
            alert('Hubo un problema, por favor vuelva a intentarlo más tarde');
        }
    });

}


$("#filterDp").change(function () {
    //split the current value of searchInput
    var data = this.value.split(" ");
    console.log(data[0]);
    //create a jquery object of the rows
    var jo = $("#fbody").find("tr");
    if (this.value == "") {
        jo.show();
        return;
    }
    //hide all the rows
    jo.hide();

    //Recusively filter the jquery object to get results.
    jo.filter(function (i, v) {
        var $t = $(this);
        for (var d = 0; d < data.length; ++d) {
            if ($t.text().indexOf(data[d]) > -1 || data[d] == '--Porfavor') {
                return true;
            }
        }
        return false;
    })
        //show the rows that match.
        .show();
}).focus(function () {
    this.value = "";
    $(this).css({
        "color": "black"
    });
    $(this).unbind('focus');
}).css({
    "color": "#C0C0C0"
});