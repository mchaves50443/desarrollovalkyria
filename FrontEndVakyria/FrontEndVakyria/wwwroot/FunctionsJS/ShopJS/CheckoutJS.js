﻿$(document).ready(function () {
    CargarItems();
});

function CargarItems() {
    //send ajax request
    $("#LoadingImage").show();
    $.ajax({
        type: 'POST',
        url: '/Shop/CargarListaItems',
        data: {

        },
        dataType: 'json',
        success: function (data) {

            console.log(data);

            var subtotal = 0;
            var realTotal = 0;
            for (var i = 0; i < data.length; i++) {
                subtotal = subtotal + data[i].precio;

                var precioTotalArticulo = data[i].precio * data[i].cantidad;
                var newLiElement = '<li>' + data[i].nombre + '<i>     X ' + data[i].cantidad + '</i> <span>₡' + precioTotalArticulo + '</span></li><div name="codigo" style="display:none;">' + data[i].idItem + '</div></div><div name="tipoItem" style="display:none;">' + data[i].tipoItem + '</div></div><div name="precioItem" style="display:none;">' + precioTotalArticulo + '</div></div><div name="cantidadItem" style="display:none;">' + data[i].cantidad + '</div></div>';
                $("#listaItems").append(newLiElement);

                realTotal = realTotal + precioTotalArticulo;
            }

            var newLiSubTotal = '<li>Subtotal <span>₡' + realTotal + ';</span></li>';
            $("#totales").append(newLiSubTotal);
            var newLiTotal = '<li><strong>Total</strong> <span id="subtotal"><strong>₡' + realTotal + '</strong></span></li>';
            $("#totales").append(newLiTotal);

            $("#LoadingImage").hide();

        },
        error: function (data) {
            alert('No se cargaron los productos relacionados');
        }
    });
}


function RegistrarVenta() {
    //get data
    var nombre = $("#name_on_card").val();
    var apellidos = $("#ApellidosUsuario").val();
    var canton = $("#Canton").val();
    var provincia = $("#Provincia").val();
    var distrito = $("#Distrito").val();
    var direccion = $("#Direccion").val();
    var zip = $("#CodigoPostal").val();
    var correo = $("#Correo").val();
    var telefono = $("#Telefono").val();
    //var tipotarjeta = $("#CreditCardType").val();
    var card_number = $("#card_number").val();
    var exp_month = $("#exp_month").val();
    var exp_year = $("#exp_year").val();
    var cvv = $("#cvv").val();
    var total = $("#subtotal").text();
    total = total.replace("₡", "");

    var codigo = '';
    $("div[name='codigo']").each(function () {
        codigo = codigo + $(this).text() + "|";
    });


    var tipoItem = '';
    $("div[name='tipoItem']").each(function () {
        tipoItem = tipoItem + $(this).text() + "|";
    });


    var cantidadItem = '';
    $("div[name='cantidadItem']").each(function () {
        cantidadItem = cantidadItem + $(this).text() + "|";
    });

    var precioItem = '';
    $("div[name='precioItem']").each(function () {
        precioItem = precioItem + $(this).text() + "|";
    });
    


    /*
     * 
     * string provincia, string canton,
            string distrito, string direccion, string nombre, string apellidos, string correo,
            string telefono, string total, string codigo, string zip,
            string credit_card_number,string credit_card_security_code_number,string exp_month, string exp_year
     * 
     */

    //send ajax request
    $("#myModal").modal({ backdrop: 'static' });
    //$('#myModal').modal('show');

    $.ajax({
        type: 'POST',
        url: '/Shop/RegistrarVenta',
        data: {
            provincia: provincia,
            canton: canton,
            distrito: distrito,
            direccion: direccion,
            nombre: nombre,
            apellidos: apellidos,
            correo: correo,
            telefono: telefono,
            total: total,
            codigo: codigo,
            tipoItem: tipoItem,
            cantidadItem: cantidadItem,
            precioItem: precioItem,
            zip: zip,
            credit_card_number: card_number,
            credit_card_security_code_number: cvv,
            exp_month: exp_month,
            exp_year: exp_year

        },
        dataType: 'json',
        success: function (data) {
            console.log(data);
            if (data == "success") {
                //alert("¡Compra realizada exitosamente!");
                $('#myModal').modal("hide");
                window.location.href = '../Shop/CheckoutValid';
            } else {
                $('#myModal').modal("hide");
                window.location.href = '../Shop/CheckoutError';
            }
           
           
        },
        error: function (data) {
            alert('Ha habido un error, por favor intente de nuevo.');
            $("#LoadingImage").hide();
        }
    });

}

function TieneValores() {

    var nombre = $("#name_on_card").val();
    var apellidos = $("#ApellidosUsuario").val();
    var canton = $("#Canton").val();
    var provincia = $("#Provincia").val();
    var distrito = $("#Distrito").val();
    var direccion = $("#Direccion").val();
    var zip = $("#CodigoPostal").val();
    var correo = $("#Correo").val();
    var telefono = $("#Telefono").val();
    //var tipotarjeta = $("#CreditCardType").val();
    var card_number = $("#card_number").val();
    var exp_month = $("#exp_month").val();
    var exp_year = $("#exp_year").val();
    var cvv = $("#cvv").val();
    var total = $("#subtotal").text();
    total = total.replace("₡", "");

    if (nombre != 'undefined' && nombre != null && nombre != '' &&
        apellidos != 'undefined' && apellidos != null && apellidos != '' &&
        correo != 'undefined' && correo != null && correo != '' &&
        card_number != 'undefined' && card_number != null && card_number != '' &&
        total != 'undefined' && total != null && total != '' &&
        exp_year != 'undefined' && exp_year != null && exp_year != '' &&
        cvv != 'undefined' && cvv != null && cvv != '' &&
        exp_month != 'undefined' && exp_month != null && exp_month != '') {
        return true;
    } else {
        alert('Hace falta información para realizar la compra');
        return false;

    }

}
$('#btnRealicePago').click(function () {
    //cardFormValidate();
    if (TieneValores()) {
        RegistrarVenta();
    }
});


function validateTransaction() {
	var valid = true;
	$(".demoInputBox").css('background-color', '');
	var message = "";

	var cardHolderNameRegex = /^[a-z ,.'-]+$/i;
	var cvvRegex = /^[0-9]{3,3}$/;

	var cardHolderName = $("#card-holder-name").val();
	var cardNumber = $("#card-number").val();
	var cvv = $("#cvv").val();

	if (cardHolderName == "" || cardNumber == "" || cvv == "") {
		message += "<div>All Fields are Required.</div>";
		if (cardHolderName == "") {
			$("#card-holder-name").css('background-color', 'white');
		}
		if (cardNumber == "") {
			$("#card-number").css('background-color', 'white');
		}
		if (cvv == "") {
			$("#cvv").css('background-color', 'white');
		}
		valid = false;
	}

	if (cardHolderName != "" && !cardHolderNameRegex.test(cardHolderName)) {
		message += "<div>Card Holder Name is Invalid</div>";
		$("#card-holder-name").css('background-color', 'white');
		valid = false;
	}

	if (cardNumber != "") {
		$('#card-number').validateCreditCard(function (result) {
			if (!(result.valid)) {
				message += "<div>Card Number is Invalid</div>";
				$("#card-number").css('background-color', 'white');
				valid = false;
			}
		});
	}

	else if (cvv != "" && !cvvRegex.test(cvv)) {
		message += "<div>CVV is Invalid</div>";
		$("#cvv").css('background-color', 'white');
		valid = false;
	}

	if (message != "") {
		$("#error-message").show();
		$("#error-message").html(message);
	}
	return valid;
}



function cardFormValidate() {
    var cardValid = 0;

    //card number validation
    $('#card_number').validateCreditCard(function (result) {
        if (result.valid) {
            $("#card_number").removeClass('required');
            cardValid = 1;
        } else {
            $("#card_number").addClass('required');
            cardValid = 0;
        }
    });

    //card details validation
    var cardName = $("#name_on_card").val();
    var expMonth = $("#expiry_month").val();
    var expYear = $("#expiry_year").val();
    var cvv = $("#cvv").val();
    var regName = /^[a-z ,.'-]+$/i;
    var regMonth = /^01|02|03|04|05|06|07|08|09|10|11|12$/;
    var regYear = /^2017|2018|2019|2020|2021|2022|2023|2024|2025|2026|2027|2028|2029|2030|2031$/;
    var regCVV = /^[0-9]{3,3}$/;
    if (cardValid == 0) {
        $("#card_number").addClass('required');
        $("#card_number").focus();
        return false;
    } else if (!regMonth.test(expMonth)) {
        $("#card_number").removeClass('required');
        $("#expiry_month").addClass('required');
        $("#expiry_month").focus();
        return false;
    } else if (!regYear.test(expYear)) {
        $("#card_number").removeClass('required');
        $("#expiry_month").removeClass('required');
        $("#expiry_year").addClass('required');
        $("#expiry_year").focus();
        return false;
    } else if (!regCVV.test(cvv)) {
        $("#card_number").removeClass('required');
        $("#expiry_month").removeClass('required');
        $("#expiry_year").removeClass('required');
        $("#cvv").addClass('required');
        $("#cvv").focus();
        return false;
    } else if (!regName.test(cardName)) {
        $("#card_number").removeClass('required');
        $("#expiry_month").removeClass('required');
        $("#expiry_year").removeClass('required');
        $("#cvv").removeClass('required');
        $("#name_on_card").addClass('required');
        $("#name_on_card").focus();
        return false;
    } else {
        $("#card_number").removeClass('required');
        $("#expiry_month").removeClass('required');
        $("#expiry_year").removeClass('required');
        $("#cvv").removeClass('required');
        $("#name_on_card").removeClass('required');
        return true;
    }
}
$(document).ready(function () {
    //card validation on input fields
    $('#paymentForm input[type=text]').on('keyup', function () {
        cardFormValidate();
    });
});