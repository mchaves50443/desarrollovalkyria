﻿$(document).ready(function () {
    CargarWorkshops();
});

function CargarWorkshops() {
    //send ajax request
    $("#LoadingImage").show();
    $.ajax({
        type: 'POST',
        url: '/Shop/CargarWorkshopsConImagen',
        data: {
 
        },
        dataType: 'json',
        success: function (data) {

            console.log(data);

            for (var i = 0; i < data.length; i++) {
                var newHTMLItem = '<div class="shop-item col-lg-4 col-md-6 col-sm-12"> <div class="inner-box"> <div class="image"> <a class="overlay-link" href="../Workshop/' + data[i].IdWorkshop + '"></a><img src="' + data[i].imgDataURL + '" alt="" /> <div class="overlay-box"> <ul class="cart-option"> <li><a href="../Workshop/' + data[i].IdWorkshop + '"><span><img src="~/images/icons/right-arrow.svg" alt="" /></span></a></li> </ul> </div> </div> <div class="lower-content"> <h3><a href="../Workshop/' + data[i].IdWorkshop + '">' + data[i].NombreWorkshop + '</a></h3> <div class="clearfix"> <div class="pull-left"> <div class="price">₡' + data[i].Precio + '</div> </div> <div class="pull-right"> <a href="../Workshop/' + data[i].IdWorkshop + '" class="cart"><span class="icon flaticon-shopping-cart-3"></span></a> </div> </div> </div> </div ></div >';
                $("#related-products-list").append(newHTMLItem); 
            }

            $("#LoadingImage").hide();

        },
        error: function (data) {
            alert('No se cargaron los productos relacionados');
        }
    });

}