﻿function cardValidator() {

    var tipoTarjeta = $("#CreditCardType").val();
    var cardNumber = $("#cardNumber").val();

    if (tipoTarjeta != undefined && tipoTarjeta != null && tipoTarjeta != "") {
        switch (tipoTarjeta) {
            case "Visa":

                var cardno = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/;
                if (cardNumber.value.match(cardno)) {
                    return true;
                }
                else {
                    alert("Not a valid Visa credit card number!");
                    return false;
                }

                break;
            case "American Express":

                var cardno = /^(?:3[47][0-9]{13})$/;
                if (cardNumber.value.match(cardno)) {
                    return true;
                }
                else {
                    alert("Not a valid American Express credit card number!");
                    return false;
                }

                break;
            case "MasterCard":

                var cardno = /^(?:5[1-5][0-9]{14})$/;
                if (cardNumber.value.match(cardno)) {
                    return true;
                }
                else {
                    alert("Not a valid Mastercard number!");
                    return false;
                }

                break;
            case "Discover":

                var cardno = /^(?:6(?:011|5[0-9][0-9])[0-9]{12})$/;
                if (cardNumber.value.match(cardno)) {
                    return true;
                }
                else {
                    alert("Not a valid Discover card number!");
                    return false;
                }

                break;
        }
    }

}

function LlamarVenta() {
    if (TieneValores() == true)
        RegistrarVenta();
}


function prueba() {
    var pais = $("#country").val();
    var provincia = $("#state").val();
    var canton = $("#city").val();
    var distrito = $("#town").val();
    var direccion = $("#address").val();
    var nombre = $("#first_name").val();
    var apellidos = $("#last_name").val();
    var correo = $("#email").val();
    var telefono = $("#phone").val();
    var total = $("#subtotal").text();

    var codigo = '';
    $("div[name='codigo']").each(function () {
        codigo = codigo + "|" + $(this).text();
    });


}


function RegistrarVenta() {
    //get data
    var pais = $("#country").val();
    var provincia = $("#state").val();
    var canton = $("#city").val();
    var distrito = $("#town").val();
    var direccion = $("#address").val();
    var zip = $("#zip_code").val();
    var nombre = $("#first_name").val();
    var apellidos = $("#last_name").val();
    var correo = $("#email").val();
    var telefono = $("#phone").val();
    var total = $("#subtotal").text();

    var codigo = '';
    $("div[name='codigo']").each(function () {
        codigo = codigo + $(this).text() + "|";
    });

    //send ajax request
    $("#LoadingImage").show();
    $.ajax({
        type: 'POST',
        url: '/CarritoCompra/RegistrarVenta',
        data: {
            pais: pais,
            provincia: provincia,
            canton: canton,
            distrito: distrito,
            direccion: direccion,
            nombre: nombre,
            apellidos: apellidos,
            correo: correo,
            telefono: telefono,
            total: total,
            codigo: codigo,
            zip: zip
        },
        dataType: 'json',
        success: function (data) {
            alert("¡Compra realizada exitosamente!");
            $("#LoadingImage").hide();
            window.location.href = '../MisProgramas/MisProgramas';
        },
        error: function (data) {
            alert('Ha habido un error, por favor intente de nuevo.');
            $("#LoadingImage").hide();
        }
    });

}

function TieneValores() {

    var nombre = $("#first_name").val();
    var apellidos = $("#last_name").val();
    var correo = $("#email").val();
    var tipotarjeta = $("#CreditCardType").val();
    var numTarjeta = $("#cardNumber").val();
    var codigoTarjeta = $("#card_code").val();
    var anoVencimiento = $("#year_venc").val();
    var mesVencimiento = $("#mes_venc").val();
    var total = $("#subtotal").text();

    var codigo = '';
    $("div[name='codigo']").each(function () {
        codigo = codigo + "|" + $(this).text();
    });

    console.log(mesVencimiento);

    if (nombre != 'undefined' && nombre != null && nombre != '' &&
        apellidos != 'undefined' && apellidos != null && apellidos != '' &&
        correo != 'undefined' && correo != null && correo != '' &&
        tipotarjeta != 'undefined' && tipotarjeta != null && tipotarjeta != '' &&
        numTarjeta != 'undefined' && numTarjeta != null && numTarjeta != '' &&
        total != 'undefined' && total != null && total != '' &&
        codigoTarjeta != 'undefined' && codigoTarjeta != null && codigoTarjeta != '' &&
        anoVencimiento != 'undefined' && anoVencimiento != null && anoVencimiento != '' &&
        codigo != 'undefined' && codigo != null && codigo != '' &&
        mesVencimiento != 'undefined' && mesVencimiento != null && mesVencimiento != '') {
        return true;
    } else {
        alert('Hace falta información para realizar la compra');
        return false;

    }

}