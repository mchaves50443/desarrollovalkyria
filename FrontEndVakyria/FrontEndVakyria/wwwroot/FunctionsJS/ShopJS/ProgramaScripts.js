﻿$(document).ready(function () {
    CargarProgramas();
});

function CargarProgramas() {
    //send ajax request
    $("#LoadingImage").show();
    $.ajax({
        type: 'POST',
        url: '/Shop/CargarProgramasConImagen',
        data: {
 
        },
        dataType: 'json',
        success: function (data) {

            console.log(data);

            for (var i = 0; i < data.length; i++) {
                var newHTMLItem = '<div class="shop-item col-lg-4 col-md-6 col-sm-12"> <div class="inner-box"> <div class="image"> <a class="overlay-link" href="../Programa/' + data[i].IdPrograma + '"></a><img src="' + data[i].imgDataURL + '" alt="" /> <div class="overlay-box"> <ul class="cart-option"> <li><a href="../Programa/' + data[i].IdPrograma + '"><span><img src="~/images/icons/right-arrow.svg" alt="" /></span></a></li> </ul> </div> </div> <div class="lower-content"> <h3><a href="../Programa/' + data[i].IdPrograma + '">' + data[i].NombrePrograma + '</a></h3> <div class="clearfix"> <div class="pull-left"> <div class="price">₡'+data[i].Precio+'</div> </div> <div class="pull-right"> <a href="../Programa/' + data[i].IdPrograma + '" class="cart"><span class="icon flaticon-shopping-cart-3"></span></a> </div> </div> </div> </div ></div >';
                $("#related-products-list").append(newHTMLItem); 
            }

            $("#LoadingImage").hide();

        },
        error: function (data) {
            alert('No se cargaron los programas relacionados');
        }
    });

}