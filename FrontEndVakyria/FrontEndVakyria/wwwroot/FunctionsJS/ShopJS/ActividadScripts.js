﻿$(document).ready(function () {
    CargarProductos();
    
});


function loadImage(path, width, height, target) {
    $('<img src="' + path + '">').on('load',function () {
        $(this).width(width).height(height).appendTo(target);
    });
}

function CargarProductos() {
    //send ajax request
    $("#LoadingImage").show();
    $.ajax({
        type: 'POST',
        url: '/Shop/CargarActividades',
        data: {
 
        },
        dataType: 'json',
        success: function (data) {

            //console.log(data);

            for (var i = 0; i < data.length; i++) {
                var newHTMLItem = '<div class="shop-item col-lg-4 col-md-6 col-sm-12"> <div class="inner-box"> <div id="LoadImages" class="image"> <a class="overlay-link" href="../Actividad/' + data[i].IdActividades + '">  </a> <img id="dummyImage" src="" alt="" /> <div class="overlay-box"> <ul class="cart-option"> <li><a href="../Actividad/' + data[i].IdActividades + '"><span><img src="~/images/icons/right-arrow.svg" alt="" /></span></a></li> </ul> </div> </div> <div class="lower-content"> <h3><a href="../Actividad/' + data[i].IdActividades + '">' + data[i].NombreActividades + '</a></h3> <div class="clearfix"> <div class="pull-left"> <div class="price">₡' + data[i].Precio + '</div> </div> <div class="pull-right"> <a href="../Actividad/' + data[i].IdActividades + '" class="cart"><span class="icon flaticon-shopping-cart-3"></span></a> </div> </div> </div> </div ></div >';
                $("#related-products-list").append(newHTMLItem); 
            }


            $('img[id="dummyImage"]').attr("src", "/images/galleryy/comunidad-02.png");

            $("#LoadingImage").hide();

        },
        error: function (data) {
            alert('No se cargaron los productos relacionados');
        }
    });

}


//loadImage("../../../images/gallery/comunidad-02.png", 300, 300, "#LoadImages");





