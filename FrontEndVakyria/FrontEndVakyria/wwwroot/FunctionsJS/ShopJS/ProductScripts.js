﻿$(document).ready(function () {
    CargarProductos();
});

function CargarProductos() {
    //send ajax request
    $("#LoadingImage").show();
    $.ajax({
        type: 'POST',
        url: '/Shop/CargarProductosConImagen',
        data: {
 
        },
        dataType: 'json',
        success: function (data) {

            console.log(data);

            for (var i = 0; i < data.length; i++) {
                var newHTMLItem = '<div class="shop-item col-lg-4 col-md-6 col-sm-12"> <div class="inner-box"> <div class="image"> <a class="overlay-link" href="../Product/' + data[i].IdProducto + '"></a><img src="' + data[i].imgDataURL + '" alt="" /> <div class="overlay-box"> <ul class="cart-option"> <li><a href="../Product/' + data[i].IdProducto + '"><span><img src="~/images/icons/right-arrow.svg" alt="" /></span></a></li> </ul> </div> </div> <div class="lower-content"> <h3><a href="../Product/' + data[i].IdProducto + '">' + data[i].NombreProducto + '</a></h3> <div class="clearfix"> <div class="pull-left"> <div class="price">₡'+data[i].Precio+'</div> </div> <div class="pull-right"> <a href="../Product/' + data[i].IdProducto + '" class="cart"><span class="icon flaticon-shopping-cart-3"></span></a> </div> </div> </div> </div ></div >';
                $("#related-products-list").append(newHTMLItem); 
            }

            $("#LoadingImage").hide();

        },
        error: function (data) {
            alert('No se cargaron los productos relacionados');
        }
    });

}

$('#btnRealicePago').click(function () {
    //cardFormValidate();
    AgregarItemACarrito();

});


function AgregarItemACarrito() {
    //get data
    var nombre = $("#NombreProducto").val();
    var apellidos = $("#ApellidosUsuario").val();
    var canton = $("#Canton").val();
    var provincia = $("#Provincia").val();
    var distrito = $("#Distrito").val();
    var direccion = $("#Direccion").val();
    var zip = $("#CodigoPostal").val();
    var correo = $("#Correo").val();
    var telefono = $("#Telefono").val();
    //var tipotarjeta = $("#CreditCardType").val();
    var card_number = $("#card_number").val();
    var exp_month = $("#exp_month").val();
    var exp_year = $("#exp_year").val();
    var cvv = $("#cvv").val();
    var total = $("#subtotal").text();
    total = total.replace("₡", "");

    var codigo = '';
    $("div[name='codigo']").each(function () {
        codigo = codigo + $(this).text() + "|";
    });


    var tipoItem = '';
    $("div[name='tipoItem']").each(function () {
        tipoItem = tipoItem + $(this).text() + "|";
    });


    var cantidadItem = '';
    $("div[name='cantidadItem']").each(function () {
        cantidadItem = cantidadItem + $(this).text() + "|";
    });

    var precioItem = '';
    $("div[name='precioItem']").each(function () {
        precioItem = precioItem + $(this).text() + "|";
    });



    /*
     * 
     * string provincia, string canton,
            string distrito, string direccion, string nombre, string apellidos, string correo,
            string telefono, string total, string codigo, string zip,
            string credit_card_number,string credit_card_security_code_number,string exp_month, string exp_year
     * 
     */

    //send ajax request
    $("#LoadingImage").show();
    $.ajax({
        type: 'POST',
        url: '/Shop/RegistrarVenta',
        data: {
            provincia: provincia,
            canton: canton,
            distrito: distrito,
            direccion: direccion,
            nombre: nombre,
            apellidos: apellidos,
            correo: correo,
            telefono: telefono,
            total: total,
            codigo: codigo,
            tipoItem: tipoItem,
            cantidadItem: cantidadItem,
            precioItem: precioItem,
            zip: zip,
            credit_card_number: card_number,
            credit_card_security_code_number: cvv,
            exp_month: exp_month,
            exp_year: exp_year

        },
        dataType: 'json',
        success: function (data) {
            console.log(data);
            if (data == "success") {
                alert("¡Compra realizada exitosamente!");
            } else {
                alert('Ha habido un problema, por favor intente de nuevo.');
            }
            $("#LoadingImage").hide();
            //window.location.href = '../MisProgramas/MisProgramas';
        },
        error: function (data) {
            alert('Ha habido un error, por favor intente de nuevo.');
            $("#LoadingImage").hide();
        }
    });

}