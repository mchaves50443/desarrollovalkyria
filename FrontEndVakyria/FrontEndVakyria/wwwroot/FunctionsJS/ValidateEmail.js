﻿

$("#CorreoElectronico").change(function () {

    var correo = $(this).val();

    if (ValidateEmail(correo)) {

        $.ajax({
            type: 'POST',
            url: '/Login/ValidatingEmail',
            data: {
                CorreoElectronico: correo

            },
            dataType: 'json',
            success: function (data) {

                if (data == null) {
                    $(":submit").attr("disabled", false);
                    alert("El correo electrónico es válido.");
                } else {
                    $(":submit").attr("disabled", true);
                    alert("El correo electrónico se encuentra registrado.");
                }


            },
            error: function (data) {
                alert('Hubo un error, por favor intentelo de nuevo');
            }
        });

    }

    
});

function ValidateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
        return (true);
    }
    alert("El formato del correo electronico no es válido");
    return (false);
}

$("#password").change(function () {

    var password = $("#ContrasenaUsuario").val();
    var confirm = $(this).val();

    if (password == confirm) {
        //mostrar check
        alert("same pw");
        $(":submit").attr("disabled", false);
    } else {
        alert("not same pw");
        $(":submit").attr("disabled", true);
    }



});

$("#ContrasenaUsuario").change(function () {

    if (ValidatePassword($(this).val())) {
        //mostrar check
        alert("valid pw");
        $(":submit").attr("disabled", false);
    } else {
        alert("not a valid pw");
        $(":submit").attr("disabled", true);
    }

});

function ValidatePassword(chain) {
    var decimal = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
    if (decimal.test(chain)) {
      
        return true;
    }
    else {
       
        return false;
    }
}


//$('#frmRegisterUser').validate({
//    errorClass: 'help-block animation-slideDown',
//    errorElement: 'div',
//    errorPlacement: function (error, e) {
//        e.parents('.form-group > div').append(error);
//    },
//    highlight: function (e) {

//        $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
//        $(e).closest('.help-block').remove();
//    },
//    success: function (e) {
//        e.closest('.form-group').removeClass('has-success has-error');
//        e.closest('.help-block').remove();
//    },
//    rules: {
//        'NombreUsuario': {
//            required: true
//        },
//        'PrimerApellidoUsuario': {
//            required: true
//        },
//        'SegundoApellidoUsuario': {
//            required: true
//        },
//        'CedulaUsuario': {
//            required: true
//        },
//        'CorreoElectronico': {
//            required: true
//        },
//        'TelefonoUsuario': {
//            required: true
//        },
//        'ContrasenaUsuario': {
//            required: true
//        }



//    },
//    messages: {
//        'NombreUsuario': '* Campo requerido',
//        'PrimerApellidoUsuario': '* Campo requerido',
//        'SegundoApellidoUsuario': '* Campo requerido',
//        'CedulaUsuario': '* Campo requerido',
//        'CorreoElectronico': '* Campo requerido',
//        'TelefonoUsuario': '* Campo requerido',
//        'ContrasenaUsuario': '* Campo requerido'
//    }
//});