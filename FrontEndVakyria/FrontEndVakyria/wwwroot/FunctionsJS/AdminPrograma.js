﻿$("#IdPrograma").change(function () {

    ConsultaPrecio();

});

function ConsultaPrecio() {
    var IdPrograma = $("#IdPrograma").val();

    $.ajax({
        type: 'POST',
        url: '/AdminPrograma/ConsultarPorId',
        data: {
            IdPrograma: IdPrograma
        },
        dataType: 'json',
        success: function (data) {

            console.log(data);
            $("#Descripcion").val(data.Descripcion);
            $("#IdImagen").val(data.IdImagen);
            $("#NombrePrograma").val(data.NombrePrograma);
            $("#Precio").val(data.Precio);

        },
        error: function (data) {
            alert('No se pudo cargar el Programa');
        }
    });

}



//$("#IdProd").click(function () {

//    var IdProd = $(this).attr("value");

//    if (confirm("¿Desea eliminar este Programa?") == true) {

//        EliminaPrograma();
//    }

//});

function EliminaPrograma(IdProd) {


    $.ajax({
        type: 'POST',
        url: '/AdminPrograma/EliminarPrograma',
        data: {
            IdProd: IdProd
        },
        dataType: 'json',
        success: function (data) {

            window.location.href = './Consultar';
            console.log("Eliminado");

        },
        error: function (data) {
            alert('Hubo un problema, por favor vuelva a intentarlo más tarde');
        }
    });

}
