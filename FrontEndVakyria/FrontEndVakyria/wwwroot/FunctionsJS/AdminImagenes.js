﻿
function EliminaImagen(IdImagen) {


    $.ajax({
        type: 'POST',
        url: '/AdminProducto/EliminarImagen',
        data: {
            IdImagen: IdImagen
        },
        dataType: 'json',
        success: function (data) {

            window.location.href = './Imagen';
            console.log("Eliminado");

        },
        error: function (data) {
            alert('Hubo un problema, por favor vuelva a intentarlo más tarde');
        }
    });

}


$("#filterDp").change(function () {
    //split the current value of searchInput
    var data = this.value.split(" ");
    //create a jquery object of the rows
    var jo = $("#fbody").find("tr");
    if (this.value == "") {
        jo.show();
        return;
    }
    //hide all the rows
    jo.hide();

    //Recusively filter the jquery object to get results.
    jo.filter(function (i, v) {
        var $t = $(this);
        for (var d = 0; d < data.length; ++d) {
            if ($t.is(":contains('" + data[d] + "')") || data[d] == '--Porfavor Seleccionar--') {
                return true;
            }
        }
        return false;
    })
        //show the rows that match.
        .show();
}).focus(function () {
    this.value = "";
    $(this).css({
        "color": "black"
    });
    $(this).unbind('focus');
}).css({
    "color": "#C0C0C0"
});