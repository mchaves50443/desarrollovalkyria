﻿////$("#IdActividades").change(function () {

////    ConsultaActividad();

////});

$("#IdActividades").change(function () { 
    
    var IdAct = $("#IdActividades").val();
    $.ajax({
        type: 'POST',
        url: '/AdminActividad/ConsultarPorId',
        data: {
            IdActividad: IdAct
        },
        dataType: 'json',
        success: function (data) {

            console.log(data);

            $("#NombreActividades").val(data.NombreActividades);
            $("#MinInventario").val(data.MinInventario);
            $("#MaxInventario").val(data.MaxInventario);
            $("#Precio").val(data.Precio);
            $("#Fecha").val(data.Fecha);
            $("#Descripcion").val(data.Descripcion);
            $("#TipoActividad").val(data.TipoActividad);

        },
        error: function (data) {
            alert('No se pudo cargar la actividad');
            $("#TipoActividad").val("No se pudo cargar la actividad");
        }
    });

});


function EliminaProducto(IdAct) {


    $.ajax({
        type: 'POST',
        url: '/AdminActividad/EliminarActividad',
        data: {
            IdAct: IdAct
        },
        dataType: 'json',
        success: function (data) {

            window.location.href = './ListaActividades';
            console.log("Eliminado");

        },
        error: function (data) {
            alert('Hubo un problema, por favor vuelva a intentarlo más tarde');
        }
    });

}



$("#filterDp").change(function () {
    //split the current value of searchInput
    var data = this.value.split(" ");
    //create a jquery object of the rows
    var jo = $("#fbody").find("tr");
    if (this.value == "") {
        jo.show();
        return;
    }
    //hide all the rows
    jo.hide();

    //Recusively filter the jquery object to get results.
    jo.filter(function (i, v) {
        var $t = $(this);
        for (var d = 0; d < data.length; ++d) {
            if ($t.is(":contains('" + data[d] + "')") || data[d] == '--Porfavor Seleccionar--') {
                return true;
            }
        }
        return false;
    })
        //show the rows that match.
        .show();
}).focus(function () {
    this.value = "";
    $(this).css({
        "color": "black"
    });
    $(this).unbind('focus');
}).css({
    "color": "#C0C0C0"
});