﻿$('#fInicio').bootstrapMaterialDatePicker({
    format: 'YYYY-MM-DD hh:mm a',
    lang: 'es',
    weekStart: 1,
    cancelText: 'Cancelar',
    nowButton : false,
    switchOnClick : true
});

$('#fFinal').bootstrapMaterialDatePicker({
    format: 'YYYY-MM-DD hh:mm a',
    lang: 'es',
    weekStart: 1,
    cancelText: 'Cancelar',
    nowButton: false,
    switchOnClick: true
});

$(document).ready(function () {

    $("#cbAbierta").prop("checked", true);

    $("#cbCerrada").prop("checked", false);

    $(".cbSetAbierta").prop("checked", true);

    $(".cbSetAbierta").prop("checked", false);

});

$("#cbCerrada").click(function () {
    $("#cbAbierta").click();
});

$("#cbAbierta").click(function () {
    $("#cbCerrada").click();
});


$(".cbSetCerrada").click(function () {
    $(".cbSetAbierta").click();
});

$(".cbSetAbierta").click(function () {
    $(".cbSetCerrada").click();
});


//function ActualizarEstadoOrden() {
//    //send ajax request
//    $.ajax({
//        type: 'POST',
//        url: '/Admin/ActualizarEstadoOrden',
//        data: {

//        },
//        dataType: 'json',
//        success: function (data) {

//            alert("Se ha actualizado el estado de la orden.");
//        },
//        error: function (data) {
//            alert('Hubo problemas, por favor inténtelo de nuevo.');
//        }
//    });
//}