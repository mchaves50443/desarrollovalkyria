﻿$("#IdWorkshop").change(function () {

    ConsultaPrecio();

});

function ConsultaPrecio() {
    var IdWorkshop = $("#IdWorkshop").val();

    $.ajax({
        type: 'POST',
        url: '/AdminWorkshop/ConsultarPorId',
        data: {
            IdWorkshop: IdWorkshop
        },
        dataType: 'json',
        success: function (data) {

            console.log(data);
            $("#MinInventario").val(data.MinInventario);
            $("#MaxInventatio").val(data.MaxInventatio);
            $("#Descripcion").val(data.Descripcion);
            $("#IdImagen").val(data.IdImagen);
            $("#NombreWorkshop").val(data.NombreWorkshop);
            $("#Precio").val(data.Precio);
            $("#Fecha").val(data.Fecha);

        },
        error: function (data) {
            alert('No se pudo cargar el workshop');
            $("#TipoWorkshop").val("No se pudo cargar el workshop");
        }
    });

}



//$("#IdWork").click(function () {

//    EliminaWorkshop();

//});

function EliminaWorkshop(IdWork) {

    $.ajax({
        type: 'POST',
        url: '/AdminWorkshop/EliminarWorkshop',
        data: {
            IdWork: IdWork
        },
        dataType: 'json',
        success: function (data) {

            window.location.href = './Consultar';
            console.log("Eliminado");

        },
        error: function (data) {
            alert('Hubo un problema, por favor vuelva a intentarlo más tarde');
        }
    });

}

$("#filterDp").change(function () {
    //split the current value of searchInput
    var data = this.value.split(" ");
    //create a jquery object of the rows
    var jo = $("#fbody").find("tr");
    if (this.value == "") {
        jo.show();
        return;
    }
    //hide all the rows
    jo.hide();

    //Recusively filter the jquery object to get results.
    jo.filter(function (i, v) {
        var $t = $(this);
        for (var d = 0; d < data.length; ++d) {
            if ($t.is(":contains('" + data[d] + "')") || data[d] == '--Porfavor Seleccionar--') {
                return true;
            }
        }
        return false;
    })
        //show the rows that match.
        .show();
}).focus(function () {
    this.value = "";
    $(this).css({
        "color": "black"
    });
    $(this).unbind('focus');
}).css({
    "color": "#C0C0C0"
});