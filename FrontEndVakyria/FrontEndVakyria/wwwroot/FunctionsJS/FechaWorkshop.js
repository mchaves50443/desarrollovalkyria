﻿$('#feInicio').bootstrapMaterialDatePicker({
    format: 'YYYY-MM-DD hh:mm a',
    lang: 'es',
    weekStart: 1,
    cancelText: 'Cancelar',
    nowButton: false,
    switchOnClick: true
});