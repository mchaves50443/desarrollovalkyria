﻿using FrontEndVakyria.Modelos_BD;
using System.Collections.Generic;


namespace FrontEndVakyria.Entities
{
    public partial class TipoImagen
    {
        public TipoImagen()
        {
        }

        public int IdImagen { get; set; }
        public string NombreImagen { get; set; }
        public byte[] DatosImagen { get; set; }
        public string imgDataURL { get; set; }

    }
}
