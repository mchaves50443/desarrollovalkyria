﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEndVakyria.Entities
{
    public class Item
    {
        public int IdItem { get; set; }
        public string Nombre { get; set; }
        public string TipoItem { get; set; }
        public int Cantidad { get; set; }
        public decimal Precio { get; set; }

    }
}
