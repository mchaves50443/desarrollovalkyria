﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEndVakyria.Entities
{
    public class ProgramaImagen
    {
        public long IdPrograma { get; set; }
        public string NombrePrograma { get; set; }
        public string Descripcion { get; set; }
        public decimal Precio { get; set; }
        public int? IdImagen { get; set; }
        public byte[] DatosImagen { get; set; }
        public string imgDataURL { get; set; }
    }
}
