﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEndVakyria.Entities
{
    public class Cart
    {
        List<ProductoImagen> ListaProductos { get; set; }
        List<WorkshopImagen> ListaWorkshops { get; set; }
    }
}
