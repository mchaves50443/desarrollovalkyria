﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEndVakyria.Entities
{
    public class WorkshopImagen
    {
        public int IdWorkshop { get; set; }
        public string NombreWorkshop { get; set; }
        public int MinInventario { get; set; }
        public int MaxInventatio { get; set; }
        public decimal Precio { get; set; }
        public DateTime Fecha { get; set; }
        public string Descripcion { get; set; }
        public int? IdImagen { get; set; }
        public byte[] DatosImagen { get; set; }
        public string imgDataURL { get; set; }

    }
}
