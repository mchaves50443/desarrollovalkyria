﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEndVakyria.Entities
{
    public class ProductoImagen
    {
        public int IdProducto { get; set; }
        public string NombreProducto { get; set; }
        public string Descripcion { get; set; }
        public string TipoProducto { get; set; }
        public int CantInventario { get; set; }
        public decimal Precio { get; set; }
        public int? IdImagen { get; set; }
        public byte[] DatosImagen { get; set; }
        public string imgDataURL { get; set; }

    }
}
