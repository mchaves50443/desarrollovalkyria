﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FrontEndVakyria.Entities
{
    public class Orden
    {
        public long IdOrden { get; set; }
        public DateTime fechaOrden { get; set; }
        public List<Item> listaItems { get; set; }
        public string correoElectronico { get; set; }
        public string  telefono { get; set; }
        public string direccionCompleta { get; set; }
        public int cantidad { get; set; }
        public decimal total { get; set; }
        public byte? estado { get; set; }


    }
}
