## README

Acá se van a documentar la información relacionada al sitema Valkyria

## Para qué es este repositorio?

•	Repositorio para el desarrollo del sistema del proyecto Valkyria como parte de proyecto de graduación de la Universidad Fidélitas
•	Carrera: Ingeniería en Sistemas de Información
•	Version 1.0

## Notas generales
 Nombre del proyecto: Proyecto Valkyria
 Integrantes:

* Moisés Chaves
* Randy Delgado
* Luis Salas
* Fabian Zepeda

## Cómo instalar/clonar el repositorio en el equipo de desarrollo?
•	Se necesita tener instalado Visual Studio 2019
•	Un equipo con Windows 10 actualizado
•	Tener instalado y configurado MySQL Workbench en los equipos
•	Tener instalado Sourcetree y configurado con la misma cuenta de Bitbucket a la que está relacionada éste proyecto
•	Desde el siguiente URL: https://bitbucket.org/moisoccg/sistemavalkyria/src/master/
•	Logueado con la cuenta de Bitbucket se puede dar click en el botón de "Clone" y se da la opción de abrir con Sourcetree
•	En Sourcetree se procederá a seleccionar la dirección en el equipo donde se desea clonar el repositorio
•	Asegurarse de ingresar las cuentas correctas y listo

## Guías de contribución
* Escribir pruebas
* Revisión del código

## Con quién reportar problemas?
* Moisés Chaves
* Randy Delgado
* Luis Salas
* Fabian Zepeda
