
-- CREACION DE TABLAS

CREATE TABLE `BD_ProyectoValkyria`.`Imagen` (
  `idImagen` INT NOT NULL AUTO_INCREMENT,
  `nombreImagen` VARCHAR(100) NOT NULL,
  `datosImagen` MEDIUMBLOB NOT NULL,
  PRIMARY KEY (`idImagen`),
  UNIQUE INDEX `idImagen_UNIQUE` (`idImagen` ASC) VISIBLE)
COMMENT = 'Tabla para mantener las imagenes de cada producto';

CREATE TABLE `BD_ProyectoValkyria`.`Producto` (
  `idProducto` INT NOT NULL AUTO_INCREMENT,
  `nombreProducto` VARCHAR(50) NOT NULL,
  `descripcion` VARCHAR(150) NULL,
  `tipoProducto` TINYINT(2) NOT NULL,
  `cantInventario` INT NOT NULL,
  `precio` DECIMAL NOT NULL,
  `idImagen` INT NULL,
  PRIMARY KEY (`idProducto`),
  UNIQUE INDEX `idProducto_UNIQUE` (`idProducto` ASC) VISIBLE,
  INDEX `idImagen_idx` (`idImagen` ASC) VISIBLE,
  CONSTRAINT `idImagen`
    FOREIGN KEY (`idImagen`)
    REFERENCES `BD_ProyectoValkyria`.`Imagen` (`idImagen`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


CREATE TABLE `BD_ProyectoValkyria`.`LogErrores` (
  `idLog` INT NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(100) NOT NULL,
  `fecha` DATE NOT NULL,
  `modulo` VARCHAR(45) NOT NULL,
  `accion` VARCHAR(45) NOT NULL,
  `usuario` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idLog`));


CREATE TABLE `BD_ProyectoValkyria`.`FacturaDetalle` (
  `idFacturaDetalle` INT NOT NULL AUTO_INCREMENT,
  `idFactura` BIGINT NULL,
  `idPrograma` BIGINT NULL,
  `idProducto` INT NULL,
  `idWorkshop` INT NULL,
  `idActividad` BIGINT NULL,
  `cantidad` INT NULL,
  `precio` DECIMAL NULL,
  PRIMARY KEY (`idFacturaDetalle`),
  UNIQUE INDEX `idFacturaDetalle_UNIQUE` (`idFacturaDetalle` ASC) VISIBLE,
  INDEX `idPrograma_idx` (`idPrograma` ASC) VISIBLE,
  INDEX `idProducto_idx` (`idProducto` ASC) VISIBLE,
  INDEX `idWorkshop_idx` (`idWorkshop` ASC) VISIBLE,
  INDEX `idFactura_idx` (`idFactura` ASC) VISIBLE,
  INDEX `idActividad_idx` (`idActividad` ASC) VISIBLE,
  CONSTRAINT `idFactura`
    FOREIGN KEY (`idFactura`)
    REFERENCES `BD_ProyectoValkyria`.`Factura` (`idFactura`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idPrograma`
    FOREIGN KEY (`idPrograma`)
    REFERENCES `BD_ProyectoValkyria`.`Programa` (`idPrograma`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idProducto`
    FOREIGN KEY (`idProducto`)
    REFERENCES `BD_ProyectoValkyria`.`Producto` (`idProducto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idWorkshop`
    FOREIGN KEY (`idWorkshop`)
    REFERENCES `BD_ProyectoValkyria`.`Workshop` (`idWorkshop`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idActividad`
    FOREIGN KEY (`idActividad`)
    REFERENCES `BD_ProyectoValkyria`.`Actividades` (`idActividades`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

  `idFacturaDetalle` INT NOT NULL AUTO_INCREMENT,
  `idFactura` INT NOT NULL,
  `idPrograma` BIGINT NULL,
  `idProducto` INT NULL,
  `idWorkshop` INT NULL,
  `idActividad` INT NULL,
  `cantidad` INT NOT NULL,
  `precio` DECIMAL NULL,
  PRIMARY KEY (`idFacturaDetalle`),
  UNIQUE INDEX `idFacturaDetalle_UNIQUE` (`idFacturaDetalle` ASC) VISIBLE,
  INDEX `idPrograma_idx` (`idPrograma` ASC) VISIBLE,
  INDEX `idProducto_idx` (`idProducto` ASC) VISIBLE,
  INDEX `idWorkshop_idx` (`idWorkshop` ASC) VISIBLE,
  CONSTRAINT `idFactura`
    FOREIGN KEY ()
    REFERENCES `BD_ProyectoValkyria`.`Factura` ()
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idPrograma`
    FOREIGN KEY (`idPrograma`)
    REFERENCES `BD_ProyectoValkyria`.`Programa` (`idPrograma`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idProducto`
    FOREIGN KEY (`idProducto`)
    REFERENCES `BD_ProyectoValkyria`.`Producto` (`idProducto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idWorkshop`
    FOREIGN KEY (`idWorkshop`)
    REFERENCES `BD_ProyectoValkyria`.`Workshop` (`idWorkshop`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idActividad`
    FOREIGN KEY ()
    REFERENCES `BD_ProyectoValkyria`.`Actividades` ()
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- CAMBIOS

ALTER TABLE `BD_ProyectoValkyria`.`Workshop` 
ADD COLUMN `idImagen` INT NULL AFTER `descripcion`,
CHANGE COLUMN `precio` `precio` DECIMAL(10,0) NOT NULL ,
CHANGE COLUMN `descripcion` `descripcion` VARCHAR(300) NOT NULL ,
ADD INDEX `idImagen_idx` (`idImagen` ASC) VISIBLE;
;
ALTER TABLE `BD_ProyectoValkyria`.`Workshop` 
ADD CONSTRAINT `idImagen`
  FOREIGN KEY (`idImagen`)
  REFERENCES `BD_ProyectoValkyria`.`Imagen` (`idImagen`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


-- CONSULTAS


-- INSERCIONES

INSERT INTO Producto (nombreProducto, descripcion, tipoProducto, cantInventario, precio, idImagen) 
VALUES('Camiseta Valkyria','Ropa para utilizar cuando se entrena',1,10,12500,null);


--TRIGGERS
CREATE TRIGGER default_estado_factura 
    before insert ON Factura
    FOR EACH ROW
 SET NEW.estado = 0;
